function [barcodeGen] = gen_barcodes(kymoData, sets)
  % gen_barcodes
  % Generates experimental barcodes that are aligned to theory
  %     Args:
  %         kymoData: struct
  %         sets: SETTINGS
  %     Returns:
  %         barcodeGen: output structure

  %     disp('Starting generating barcodes...')
  barcodeGen = cell(1, length(kymoData));
  %     tic
  for i = 1:length(kymoData)
    % aligned kymo mask
    alignedKymBitMask = kymoData{i}.alignedKymBitMask;

    % assign nan's to the values that are not signal
    alignedKymo = kymoData{i}.alignedKymo;
    alignedKymo(~alignedKymBitMask) = nan;

    % find left and right indices
    [~, leftEdgeIdxs] = max(alignedKymBitMask, [], 2);
    [~, rightEdgeIdxs] = min(alignedKymBitMask(:, max(leftEdgeIdxs):end), [], 2);
    rightEdgeIdxs = rightEdgeIdxs + max(leftEdgeIdxs) - 1;

    % mean left and right indexes
    leftEdgeIdx = round(nanmean(leftEdgeIdxs));
    rightEdgeIdx = round(nanmean(rightEdgeIdxs));

    % Compute the barcode by averaging over non-nan values
    if size(alignedKymo, 1) > 1
      rawBarcode = nanmean(alignedKymo, 1);
    else
      rawBarcode = alignedKymo;
    end

    %         figure
    %         image(kymoData{i}.unalignedKymo)
    %         figure
    %         image(kymoData{i}.alignedKymo)
    %         figure
    %         plot(nanmean(kymoData{i}.alignedKymo, 1))

    % alternatively compute this from using left and right indices
    rawBg = nanmean(kymoData{i}.backgroundKym(:));
    % background mean
    %barcodeGenData{i}.bgMeanApprox = rawBg;

    % barcode time series is estimated to last from mean left ot mean
    % right
    barcodeIdxs = leftEdgeIdx:rightEdgeIdx;
    barcodeGen{i}.rawBarcode = rawBarcode(barcodeIdxs);

    % we note the left right indexes and the background
    barcodeGen{i}.lE = leftEdgeIdx;
    barcodeGen{i}.rE = rightEdgeIdx;
    barcodeGen{i}.lEdgeIdxs = leftEdgeIdxs;
    barcodeGen{i}.rEdgeIdxs = rightEdgeIdxs;
    barcodeGen{i}.rawBg = rawBg;

    bitmaskLen = length(barcodeGen{i}.rawBarcode);
    edgeLen = round(sets.camera.sigmaEdge * sets.camera.psfnm / sets.camera.pxwnm);
    barcodeGen{i}.rawBitmask = true(1, bitmaskLen);
    barcodeGen{i}.rawBitmask([1:min(edgeLen, bitmaskLen), max(bitmaskLen + 1 - edgeLen, 1):bitmaskLen]) = false;
  end

  %     timePassed = toc;
  %     display(strcat(['All barcodes generated in ' num2str(timePassed) ' seconds']));

end
