function consensus = consensus_many_cuts(consensusPath, pathFrags, enzymes, sets)
    %% consensus_many_cuts
    % Example consensus generation from circular DNA cut any number of
    % times by restriction enzyme(s)

    
%     %% Consenus kymos
%     disp('Please select folder containing experimental consensus kymographs')
%     consensusPath = uigetdir(pwd, 'Select folder containing experimental consenus kymographs');
% 
%     %% Fragment folders present
% 
%     disp('Please select (one or more) folder(s) containing experimental fragment kymographs')
%     pathFrags = {};
%     consensus.enzymes = {};
%     while true
%         thisPath = uigetdir(pwd, 'Select (one or more) folder(s) containing experimental fragment kymographs');
%         if all(thisPath == 0); break; end
%         pathFrags{end+1} = thisPath;
%         consensus.enzymes(end+1) = inputdlg('Enzyme name', 'Enzyme name');
%     end

    consensus.enzymes = enzymes;


    %% Generate dense consensus

    consensusBarcode = Consensus.circular_consensus(consensusPath, sets);
    meanLength = length(consensusBarcode);

    %% Generate cut clusters
    
    filterSize = sets.camera.psfnm/sets.camera.pxwnm;
    
    rawLabels = {};
    clusterLabels = {};
    cutsInCluster = {};
    fragments = {};

    for i=1:length(pathFrags)
        fprintf('Aligning fragments for set %d out of %d\n', i, length(pathFrags))
        % Generate barcodes
        expKymos = Consensus.generate_aligned_kymos(pathFrags{i}, filterSize);
        expBarcodes = Consensus.gen_barcodes(expKymos, sets);
        
        % Stretch fragments to length that optimizes PCC
        fragments{i} = Consensus.stretch_fragments_optimal_cc(expBarcodes, ...
            consensusBarcode, ...
            sets.consensus.fragStretchAmount, ...
            sets.consensus.ccThreshold);
        
        fprintf('Finding cut positions for set %d out of %d\n', i, length(pathFrags))
        rawLabels{i} = Consensus.find_cuts_experiment(consensusBarcode, ...
            fragments{i}, ...
            false);
%                                                                    figure;histogram(fragmentConsensus(i).rawLabels, 80);
        [clusterLabels{i}, ...
            cutsInCluster{i}] = Consensus.gen_cut_clusters_experiment(rawLabels{i}, meanLength, sets);
    end

    megaclusterLabels = horzcat(clusterLabels{:});
    cutsInMegacluster = vertcat(cutsInCluster{:});
    megacluster = arrayfun(@(i) mod(cutsInMegacluster{i}-megaclusterLabels(i)+meanLength/2, meanLength), ...
        1:length(megaclusterLabels), 'un', 0);
    megacluster = vertcat(megacluster{:});

    %% Output dense and sparse consensuses

    consensus.barcode = consensusBarcode;
    consensus.fragments = fragments;
    consensus.labels = rawLabels;
    consensus.dots = clusterLabels;
    consensus.dotStd = std(megacluster);

end

