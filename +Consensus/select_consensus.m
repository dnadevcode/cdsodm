function [ consensus] = select_consensus( consensusStructs, consensusThresh )
    % select_consensus
    % Selectect consensus
    %     Args:
    %         consensusStructs: consensus structs
    %         sets
    %     Returns:
    %         consensus: consensus output struct
    %         sets

    
    consensus = struct();

    consensusIndex = find(consensusStructs.treeStruct.maxCorCoef>consensusThresh,1,'last');
    
    if isempty(consensusIndex)
        disp('All comparisons are below barcode cluster limit');
        return;
    end
    % number of barcodes averaged at each step
    numBars = cellfun(@(x) length(cell2mat(x)), consensusStructs.treeStruct.clusteredBar);
    % largers cluster above the threshold
    [~,idx] = max(numBars(1:consensusIndex));
    
%     consensus.rawCutLabels = consensusStructs.treeStruct.barOrientation{idx}(:,1)-1;
    
    % extract barcode
    consensus.rawBarcode = nanmean(consensusStructs.treeStruct.barcodes{idx});
	% barcode indices
    consensus.indices = cell2mat(consensusStructs.treeStruct.clusteredBar{idx});
    % bitmasks
    bitm = isnan(consensusStructs.treeStruct.barcodes{idx});
    % nonzero are only those indices that have been included significant
    % amount of times
    consensus.rawBitmask = sum(~bitm)> 3*std(sum(bitm));


end

