function [consensusBarcode, ...
          barcodesInConsensus, ...
          lengthMean, ...
          lengthVar] = circular_consensus(experimentFolder, ...
                                          sets)

                                      
    % Init settings
    filterSize = sets.camera.psfnm/sets.camera.pxwnm;
    barcodeNormalization = sets.consensus.bcNormMethod;
    ccThreshold = sets.consensus.ccThreshold;
    
    
    % Generate barcodes
    expKymos = Consensus.generate_aligned_kymos(experimentFolder, filterSize);
    expBarcodes = Consensus.gen_barcodes(expKymos, sets);
    
    
    % Generate consensus to filter experimental barcodes.
    preConsensusBarcodes = Consensus.stretch_barcodes(expBarcodes, 0);
    consensusTree = Consensus.gen_consensus(preConsensusBarcodes, ...
        barcodeNormalization);
    consensus = Consensus.select_consensus(consensusTree, ...
        ccThreshold);
    
    
    % Get new length mean
    [postConsensusBarcodes, ...
     lengthMean, ...
     lengthVar] = Consensus.stretch_barcodes(expBarcodes(consensus.indices), 0);
 
    
    % Generate refined consensus...
    consensusTree = Consensus.gen_consensus(postConsensusBarcodes, ...
        barcodeNormalization);
    consensus = Consensus.select_consensus(consensusTree, ...
        ccThreshold);
    
                                    
    % Output consensus barcodes
    barcodesInConsensus = expBarcodes(consensus.indices);
    consensusBarcode = consensus.rawBarcode;
    
end

