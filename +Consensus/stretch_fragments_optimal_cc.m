function [barcodeGen] = stretch_fragments_optimal_cc(barcodeGen, ...
    consensusBarcode, ...
    stretchFraction, ...
    ccThreshold)
  import SignalRegistration.unmasked_pcc_corr

  lengthPx = length(consensusBarcode);
  tmpBitmask = true(1, lengthPx);

  stretchFrac = 2-stretchFraction:0.01:stretchFraction;

  stretchBarcode = cell(length(barcodeGen), length(stretchFrac));
  stretchBitmask = cell(length(barcodeGen), length(stretchFrac));

  cMat = nan(length(barcodeGen), length(stretchFrac));

  for i = 1:length(barcodeGen)
    barcode = barcodeGen{i}.rawBarcode;
    bitmask = barcodeGen{i}.rawBitmask;

    fragLen = length(barcode);

    for j = 1:length(stretchFrac)
      v = linspace(1, fragLen, min(round(fragLen * stretchFrac(j)), lengthPx));
      stretchBarcode{i, j} = interp1(barcode, v);
      stretchBitmask{i, j} = bitmask(round(v));

      xcorrs = unmasked_pcc_corr(stretchBarcode{i, j}, ...
        consensusBarcode, ...
        stretchBitmask{i, j});
% max(xcorrs(:))
      cMat(i, j) = max(xcorrs(:));
    end

  end

  [maxC, optStretch] = max(cMat, [], 2);
  barcodeGen(maxC < ccThreshold) = [];

  for i = 1:length(barcodeGen)
    barcodeGen{i}.stretchedBarcode = stretchBarcode{i, optStretch(i)};
    barcodeGen{i}.stretchedBitmask = stretchBitmask{i, optStretch(i)};
  end

end
