function [clusterMeans, ...
          cutsInCluster, ...
          numCutsInCluster] = gen_cut_clusters_experiment(cutLabels, ...
                                                          lengthInPx, ...
                                                          sets)
                                                      
    import BallsInBoxes.simulate_balls_in_boxes
    
    subPix = 10; % Subpixel count
    
    binWidth = sets.bab.binWidth*subPix;
    
    boxCount = lengthInPx*subPix;
    startBoxCount = boxCount;
    numBalls = length(cutLabels);
    
    %%
    
    if binWidth >= boxCount
        complexMean = angle(sum(exp(2i*pi/lengthInPx*cutLabels)));
        clusterMeans = mod(lengthInPx*(complexMean/2/pi+1), lengthInPx);
        numCutsInCluster = length(cutLabels);
        return
    end
    
    %% 
    
    % Count number of cuts in each bin
    windowVec = [ones(1, binWidth) zeros(1, boxCount - binWidth)];
    experimentalBalls = mod(round(cutLabels*subPix), boxCount);
    experimentalBoxes = arrayfun(@(x) sum(experimentalBalls==x), 0:boxCount-1);
    experimentalBins = ifft(conj(fft(windowVec)) .* fft(experimentalBoxes));
            
    %% Balls and boxes
    
    clusterMeans = [];
    cutsInCluster = {};
    numCutsInCluster = [];
    
    while numBalls > 1 && boxCount > binWidth
      
%       figure;plot(experimentalBins)
        
        ballsInBoxes = simulate_balls_in_boxes(numBalls, ...
                                               boxCount, ...
                                               binWidth, ...
                                               sets.bab.numBallsAndBoxes);
        expectedClusterMean = mean(ballsInBoxes);
        expectedClusterStd = std(ballsInBoxes);
        numCutsThreshold = expectedClusterMean + sets.bab.clusterSigma*expectedClusterStd;
        
        [numMaxBin, idxMaxBin] = max(experimentalBins);
        
%         disp(num2str([numMaxBin numCutsThreshold]))
        
        if numMaxBin >= numCutsThreshold
            
            if idxMaxBin == 1
                clusterWindow = (startBoxCount + .5) <= cutLabels*subPix | ...
                                cutLabels*subPix < (idxMaxBin + binWidth - .5);
                experimentalBoxes(idxMaxBin:idxMaxBin+binWidth-1) = 0;
            elseif idxMaxBin + binWidth - 1 <= startBoxCount
                clusterWindow = (idxMaxBin - .5) <= cutLabels*subPix & ...
                                cutLabels*subPix < (idxMaxBin + binWidth - .5);
                experimentalBoxes(idxMaxBin:idxMaxBin+binWidth-1) = 0;
            else
                clusterWindow = (idxMaxBin - .5) <= cutLabels*subPix | ...
                                cutLabels*subPix < (binWidth - startBoxCount + idxMaxBin - .5);
                experimentalBoxes(idxMaxBin:end) = 0;
                if idxMaxBin ~= startBoxCount - binWidth + 1
                    experimentalBoxes(1:binWidth-end+idxMaxBin-1) = 0;
                end
            end
            
            clusterPositions = cutLabels(clusterWindow);
            
            cutsInCluster = [cutsInCluster; clusterPositions];
            
            complexMean = angle(sum(exp(2i*pi/lengthInPx*clusterPositions)));
            clusterMean = mod(lengthInPx*(complexMean/2/pi+1), lengthInPx);
            clusterMeans = [clusterMeans clusterMean];
            numCutsInCluster = [numCutsInCluster length(clusterPositions)];
            
            numBalls = numBalls - int64(numMaxBin);
            boxCount = boxCount - binWidth;
            
            experimentalBins = ifft(conj(fft(windowVec)) .* fft(experimentalBoxes));
            
        else
            break
        end
    end
    
    
    
    
end

