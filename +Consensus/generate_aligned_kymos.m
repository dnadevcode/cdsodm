function [ kymoData ] = generate_aligned_kymos( dirKymo, filterSize )
    % generate_aligned_kymos
    % Generates aligned kymos
    %     Args:
    %         sets: theory sets
    %     Returns:
    %         expData: Output experimental data

    %% Now generate database barcodes

    % load experimental sequences. 
    listing = dir(fullfile(dirKymo, '/**/*.tif'));
%     listing = listing(36:52);
    kymoData = cell(1,length(listing));
    for i=1:length(listing)
        kymoData{i}.unalignedKymo = imread(fullfile(listing(i).folder, listing(i).name));
        kymoData{i}.name = listing(i).name;
    end

    % align kymographs
    import OptMap.KymoAlignment.apply_gaussian_blur;
    import OptMap.KymoAlignment.NRAlign.nralign
    
    for i=1:length(kymoData)
        % convert unaligned kymo to double
        kymoToAlign = double(kymoData{i}.unalignedKymo);
        

        unAlignedKymoMoleculeMask = zeros(size(kymoToAlign));
        for j=1:size(kymoToAlign,1)
            % filter the row of a kymo using imgaussfilt and filtersize
            % computed before
            
%             apply_gaussian_blur(shiftAlignedKymo, squareSmoothingWindowLen_pixels, blurSigmaWidth_pixels);
            filteredKymo = apply_gaussian_blur(kymoToAlign(j,:), round(5*filterSize), filterSize);

            % use kmeans to separate indexes in background and forward
            [idx1,~] = kmeans(zscore(filteredKymo'),2);
            % make sure indexes are unique
            [~,~,idx1] = unique(idx1,'stable');
            idx1 = idx1-1;
            %
            % find first signal index
            leftEdgeIdxs = find(idx1,1,'first');
            % find last signal index
            rightEdgeIdxs = find(idx1,1,'last');
            % update unaligned kymograph molecule mask row
            unAlignedKymoMoleculeMask(j,leftEdgeIdxs:rightEdgeIdxs)= ones(1,rightEdgeIdxs-leftEdgeIdxs+1);             
        end
        kymoMask = logical(unAlignedKymoMoleculeMask);
        kymoData{i}.unAlignedKymoMoleculeMask = unAlignedKymoMoleculeMask;
        
        [kymoData{i}.alignedKymo, ~, ~, ...
         kymoData{i}.alignedKymBitMask] = nralign(kymoToAlign, false, kymoMask);
     
        kymoData{i}.backgroundKym = kymoData{i}.alignedKymo;
        kymoData{i}.backgroundKym(kymoData{i}.alignedKymBitMask) = nan;

    end

end

