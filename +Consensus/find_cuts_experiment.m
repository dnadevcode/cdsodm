function cutPositions = find_cuts_experiment(consensusBarcode, ...
                                             barcodeGen, ...
                                             bSingleCut)
    %% find_cuts_experiment
    % Finds the position of cuts in pixels in relation to the consensus

    import SignalRegistration.unmasked_pcc_corr
    
    if nargin < 3
        bSingleCut = true;
    end
    
    barcodes = cellfun(@(x) x.stretchedBarcode,barcodeGen,'UniformOutput',false);
    bitmasks = cellfun(@(x) x.stretchedBitmask,barcodeGen,'UniformOutput',false);
    
    numBarcodes = length(barcodes);
    lengthPx = length(consensusBarcode);
    if bSingleCut
        numCuts = length(barcodes);
    else
        numCuts = 2*length(barcodes);
    end
    cutPositions = zeros(numCuts,1);

    for i=1:numBarcodes
        barcode = barcodes{i};
        bitmask = bitmasks{i};
        xcorrs = unmasked_pcc_corr(barcode, consensusBarcode, ...
            bitmask);
                          
        [~, linIdx] = max(xcorrs(:));
        [~, shift] = ind2sub(size(xcorrs), linIdx);
        
        if bSingleCut
            cutPositions(i) = shift - .5;
        else
            cutPositions(i) = shift - .5;
            cutPositions(i + numBarcodes) = mod(shift + length(barcode) - .5, lengthPx);
        end
        
    end
    
end

