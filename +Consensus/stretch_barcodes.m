function [barcodeGen, lenMean, lenVar] = stretch_barcodes(barcodeGen, predeterminedLength)
	%% Prestretch barcodes to the same length
    % convert to common length, if chosen
    
    lenBarcodes = cellfun(@(x) length(x.rawBarcode),barcodeGen);
    lenMean = mean(lenBarcodes);
    lenVar = var(lenBarcodes);
    
    if predeterminedLength ~= 0
        commonLength = predeterminedLength;
    else
        commonLength = round(lenMean);
    end

    for i=1:length(barcodeGen)  % change this to a simpler function   
        v = linspace(1, length(barcodeGen{i}.rawBarcode), commonLength);
        barcodeGen{i}.stretchedBarcode = interp1(barcodeGen{i}.rawBarcode, v);
        barcodeGen{i}.stretchedBitmask = barcodeGen{i}.rawBitmask(round(v));
%         barcodeGen{i}.rawBg = barcodeGen{i}.rawBg(round(v));
    end
    
end

