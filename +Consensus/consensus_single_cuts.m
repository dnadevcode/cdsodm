function consensus = consensus_single_cuts(pathFrags, enzymes, sets)
    %% consensus_single_cuts
    % Example consensus generation from circular DNA cut once by enzyme


    %% Fragment folders present

%     disp('Please select (one or more) folder(s) containing experimental fragment kymographs')
%     pathFrags = {};
    consensus.enzymes = enzymes;
%     while true
%         thisPath = uigetdir(pwd, 'Select (one or more) folder(s) containing experimental fragment kymographs');
%         if all(thisPath == 0); break; end
%         pathFrags{end+1} = thisPath;
%         consensus.enzymes(end+1) = inputdlg('Enzyme name', 'Enzyme name');
%     end

    %% Generate dense consensuses

    % One consensus for each fragment folder
    fragments = {};
    for i=1:length(pathFrags)
        fprintf('Aligning fragments for set %d out of %d\n', i, length(pathFrags))
        % Generate barcodes
        expKymos = Consensus.generate_aligned_kymos(pathFrags{i}, sets.camera.psfnm/sets.camera.pxwnm);
        fragments{i} = Consensus.gen_barcodes(expKymos, sets);
    end

    % Scale to length mean of all fragment folders, maybe scale to longest?
    unityBarcodeGen = horzcat(fragments{:});
    meanLength = mean(cellfun(@(x) length(x.rawBarcode), unityBarcodeGen));
    unityBarcodeGen = Consensus.stretch_barcodes(unityBarcodeGen, meanLength);
        
    % generate consensus over all fragment folders
    consensusStructs = Consensus.gen_consensus(unityBarcodeGen, sets.consensus.bcNormMethod);
    denseConsensus = Consensus.select_consensus(consensusStructs, sets.consensus.ccThreshold);
    
    meanLength = length(denseConsensus.rawBarcode);
    for i=1:length(fragments)
        fragments{i} = Consensus.stretch_barcodes(fragments{i}, meanLength);
    end


    %% Generate cut clusters    

    rawLabels = {};
    clusterLabels = {};
    cutsInCluster = {};
    for i=1:length(pathFrags)
        fprintf('Finding cut positions for set %d out of %d\n', i, length(pathFrags))
        rawLabels{i} = Consensus.find_cuts_experiment(denseConsensus.rawBarcode, ...
            fragments{i});
        [clusterLabels{i}, ...
            cutsInCluster{i}] = Consensus.gen_cut_clusters_experiment(rawLabels{i}, meanLength, sets);
    end
    
    megaclusterLabels = horzcat(clusterLabels{:});
    cutsInMegacluster = vertcat(cutsInCluster{:});
    megacluster = arrayfun(@(i) mod(cutsInMegacluster{i}-megaclusterLabels(i)+meanLength/2, meanLength), ...
        1:length(megaclusterLabels), 'un', 0);
    megacluster = vertcat(megacluster{:});

    %% Output dense and sparse consensuses

    consensus.barcode = denseConsensus.rawBarcode;
    consensus.fragments = fragments;
    consensus.labels = rawLabels;
    consensus.dots = clusterLabels;
    consensus.dotStd = std(megacluster);

end

