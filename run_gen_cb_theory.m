%% run_gen_cb_theory
% Script for pre-generating stretched theory barcodes


%%

rng(1)

sets = ini2struct('default_settings.ini');

model = Common.cb_model();
pathReference = {'D:\Arkivet\DNAdev\plasmids_FW';
  'D:\Arkivet\DNAdev\RefSeqUnpacked'};
shortestLength = 5e4;
longestLength = 8e5;


%%

rawFasta = cellfun(@(x) Import.load_fastas_from_folder(x), pathReference, 'un', 0);
rawFasta = vertcat(rawFasta{:});
seqLengths = arrayfun(@(x) length(x.Sequence), rawFasta);
rawFasta = rawFasta(seqLengths >= shortestLength & seqLengths <= longestLength);



%% Prepare output
indexedNames = arrayfun(@(x) x.Header, rawFasta, 'un', 0);
indexedBarcodes = cell(length(rawFasta), 1);

theoryFileName = 'pregen_theory.mat';
theoryFolder = 'D:\Arkivet\DNAdev\Plasmid paper\data';
theoryPath = fullfile(theoryFolder, theoryFileName);
              
% Maximum stretchfactor              
sets.experiment.lscale.meanbpnm = sets.lscale.meanbpnm*(1 + sets.reference.lengthDiffMax);              
              
if ~exist(theoryPath, 'file')
    
    disp('Generating new database of theory barcodes')

    for i=1:length(rawFasta)
        
        lengthPx = floor(length(rawFasta(i).Sequence)/(sets.camera.pxwnm/sets.lscale.meanbpnm));
        
        prob = Common.cb_theory(rawFasta(i).Sequence, ...
            sets.freeconc.concN, ...
            sets.freeconc.concY, ...
            model.yoyoBindingConstant, ...
            model.netropsinBindingConstant, ...
            0,1);
                          
        rawBarcode = Common.prob_to_pix(prob, ...
            sets.camera.psfnm/sets.lscale.meanbpnm, ...
            sets.camera.pxwnm/sets.lscale.meanbpnm, ...
            1, ...
            lengthPx);

        indexedBarcodes{i} = rawBarcode;
        
        fprintf('Finished generating barcode %d out of %d\n', i, length(rawFasta))
    end
    
else
    
    disp('Appending theory barcodes to existing database')
    
    loadedData = load(theoryPath, 'indexedNames', 'indexedBarcodes');
    indexedNames = loadedData.indexedNames;
    indexedBarcodes = loadedData.indexedBarcodes;
    
    for i=1:length(rawFasta)
        if ~ismember(rawFasta(i).Header, indexedNames)
            
            lengthPx = floor(length(rawFasta(i).Sequence)/(sets.camera.pxwnm/sets.lscale.meanbpnm));

            prob = Common.cb_theory(rawFasta(i).Sequence, ...
                sets.freeconc.concN, ...
                sets.freeconc.concY, ...
                model.yoyoBindingConstant, ...
                model.netropsinBindingConstant, ...
                0,1);

            rawBarcode = Common.prob_to_pix(prob, ...
                sets, ...
                lengthPx);
                                        
            indexedNames = [indexedNames; rawFasta(i).Header];
            indexedBarcodes = [indexedBarcodes; rawBarcodes];
        end
        fprintf('Finished generating barcode %d out of %d\n', i, length(rawFasta))
    end
    
end


save(theoryPath, 'indexedNames', 'indexedBarcodes', '-v7.3');

