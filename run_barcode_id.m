%% run_barcode_id
% Script for identifying dually labelled plasmids

%% Default settings

rng(1)

sets = ini2struct('default_settings.ini');

digits(128)

%% Prompt experiment .mat

pathConsensus = 'data\130kb.mat';
tmpFile = load(pathConsensus);
consensus = tmpFile.consensus;
consensus.lengthPx = length(consensus.barcode);
consensus.dotbars = arrayfun(@(i) Common.gen_dotbar(consensus.dots{i}, ...
  consensus.dotStd, consensus.lengthPx), 1:length(consensus.dots), 'un', 0);

pathReference = {'D:\Arkivet\DNAdev\plasmids_FW';
  'D:\Arkivet\DNAdev\RefSeqUnpacked'};
pathFFT = 'data\cb_zeromodel.mat';
pathLambda = 'data\J02459.1_Enterobacteria_phage_lambda.fasta';

%% Do free conc
disp('Computing free concentrations')
tic
[sets.freeconc.concY, ...
    sets.freeconc.concN] = Common.compute_free_conc(pathLambda, ...
  sets.freeconc.concY, ...
  sets.freeconc.concN, ...
  sets.freeconc.concDNA);
toc

%% Load reference barcodes
disp('Loading reference barcodes')
tic
referenceListing = cellfun(@(x) dir(fullfile(x, '*.f*')), pathReference, 'un', 0);
referenceListing = vertcat(referenceListing{:});
[reference.barcodes, ...
    reference.dots, ...
    reference.rawDots, ...
    reference.dotbars, ...
    reference.lengthBp, ...
    reference.names] = Common.gen_reference(referenceListing, ...
  consensus.lengthPx, ...
  consensus.dotStd, ...
  consensus.enzymes, ...
  sets);
toc

%% Calculate match-scores
disp('Calculating match-scores for densely labelled barcodes')
tic
matchScoreDense = Common.compute_alignment_score_dense(consensus.barcode, ...
  reference.barcodes);
toc
disp('Calculating match-scores for sparsely labelled barcodes')
tic
matchScoreSparse = Common.compute_alignment_score_sparse(consensus.dotbars, ...
  reference.dotbars);
toc

%% Generate random barcodes
disp('Generating random barcodes')
tic
randomBarcodes = Zeromodel.gen_random_barcodes(consensus.lengthPx, ...
  sets.zeromodel.numrand, ...
  sets.camera.psfnm / sets.camera.pxwnm, ...
  pathFFT);
toc
disp('Generating random dotbars')
tic
randomDotbars = Zeromodel.gen_random_dotbars(reference.rawDots, ...
  reference.lengthBp, ...
  consensus.lengthPx, ...
  sets, ...
  consensus.dotStd);
toc

%% Calculate p-values
disp('Calculating p-values for densely labelled barcodes')
tic
[pvalsDense, ...
    betaEVParams] = Zeromodel.compute_pvals_dense(consensus.barcode, ...
  matchScoreDense, ...
  randomBarcodes);
toc
disp('Calculating p-values for sparsely labelled barcodes')
tic
[pvalsSparse, ...
    normEVParams] = Zeromodel.compute_pvals_sparse(consensus.dotbars, ...
  reference.dotbars, ...
  matchScoreSparse, ...
  randomDotbars);
toc

%% Evaluate best matches
disp('Evaluating best matches')
tic
[pvalsDenseResampled, ...
    pvalsSparseResampled] = Common.evaluate_best_matches(consensus.barcode, ...
  consensus.dotbars, ...
  consensus.lengthPx, ...
  consensus.enzymes, ...
  pvalsDense, ...
  pvalsSparse, ...
  reference.barcodes, ...
  reference.dotbars, ...
  betaEVParams, ...
  normEVParams, ...
  sets.output.omega, ...
  sets);
toc

%% Export winners
[winnerIds, ...
    zValsBest, ...
    bestShift, ...
    zSigma] = Common.get_winners(pvalsDense, ...
  pvalsSparse, ...
  pvalsDenseResampled, ...
  pvalsSparseResampled, ...
  sets.output.omega, ...
  sets);

%

tmpDenseResampled = Common.evaluate_best_matches(consensus.barcode, ...
  consensus.dotbars, ...
  consensus.lengthPx, ...
  consensus.enzymes, ...
  pvalsDense, ...
  pvalsSparse, ...
  reference.barcodes, ...
  reference.dotbars, ...
  betaEVParams, ...
  normEVParams, ...
  1, ...
  sets);

zSigmaDense = nanstd(double(norminv(tmpDenseResampled)));

%

[~, tmpSparseResampled] = Common.evaluate_best_matches(consensus.barcode, ...
  consensus.dotbars, ...
  consensus.lengthPx, ...
  consensus.enzymes, ...
  pvalsDense, ...
  pvalsSparse, ...
  reference.barcodes, ...
  reference.dotbars, ...
  betaEVParams, ...
  normEVParams, ...
  0, ...
  sets);

zSigmaSparse = nanstd(double(norminv(tmpSparseResampled)));


%%

fprintf('Best matches:\n')

for i = 1:length(winnerIds)
  w = winnerIds(i);
  fprintf('---\n')
  fprintf('%s\n', reference.names{w})
  fprintf('P_dense: %s, P_sparse: %s, Z: %.2f +/- %.2f\n', ...
    num2str(pvalsDense{w}(bestShift(w)), 2), ...
    num2str(pvalsSparse{w}(bestShift(w)), 2), ...
    zValsBest(w), ...
    zSigma)
end

%%

save([pathConsensus(1:end-4) '_res' pathConsensus(end-3:end)], 'reference', 'winnerIds', 'pvalsDense', 'pvalsSparse', 'zSigma', 'zSigmaDense', 'zSigmaSparse', '-v7.3');
