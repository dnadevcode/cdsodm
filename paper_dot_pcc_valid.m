inputPath = 'data/emulation results/';
outputPath = 'D:/Arkivet/DNAdev/Plasmid paper/figures/pcc_vs_dotcc.eps';

seqNames = arrayfun(@(x) x.Header, rawFasta, 'un', 0);
itemList = dir(fullfile(inputPath, '*.mat'));

itemLength = zeros(length(itemList), 1);
bestd = zeros(length(itemList), 3);
bestp = zeros(length(itemList), 3);

parfor i = 1:length(itemList)
  item = load(fullfile(itemList(i).folder, itemList(i).name));
  
  consensus = item.consensus;
  reference = item.reference;
  comparisons = item.comparisons;
  
  undcc = comparisons.scoreSparse;
  pcc = comparisons.exscoreSparse;
  
  dcc = cell(size(undcc));
  
  numDotsExp = sum(cellfun(@(x) sum(x), consensus.dotbars));
  numDotsRef = sum(cellfun(@(x) sum(x), reference.dotbars), 2);
  sumDotsExp = sum(cellfun(@(x) sum(x.^2), consensus.dotbars));
  sumDotsRef = sum(cellfun(@(x) sum(x.^2), reference.dotbars), 2);
  
  for j = 1:length(undcc)
    dcc{j} = undcc{j} / sqrt(sumDotsRef(j) * sumDotsExp);
    
    if numDotsExp < 2 || numDotsRef(j) < 2
      pcc{j} = nan(size(pcc{j}));
      dcc{j} = nan(size(dcc{j}));
    end
  end
  
  [bd, did] = nanmax(cellfun(@nanmax, dcc));
  [bp, pid] = nanmax(cellfun(@nanmax, pcc));
  
  [~, dbp] = nanmax(pcc{did});
  [~, pbp] = nanmax(pcc{pid});
  
  [~, dbid] = ismember(reference.names, seqNames);
  
  bestd(i, :) = [dbid(did), dbp, strcmp(consensus.name, reference.names{did})];
  bestp(i, :) = [dbid(pid), pbp, strcmp(consensus.name, reference.names{pid})];
  
  itemLength(i) = consensus.lengthPx;
  
  disp(i)
end

fig = figure('Renderer', 'painters');
set(fig, 'Position', [0, 0 1600 600]);
scatter(bestd(:, 1), bestp(:, 1), 150, '.')
xlabel('Best matching plasmid using the alignment score D', 'interpreter', 'latex', 'FontSize', 24)
ylabel('Best matching plasmid using PCC', 'interpreter', 'latex', 'FontSize', 24)
set(gca, 'FontSize', 24);
set(gca, 'units', 'pixels');
set(gca, 'position', [150 100 1400 450]);
fig.PaperPositionMode = 'auto';
fig_pos = fig.PaperPosition;
fig.PaperSize = [fig_pos(3) fig_pos(4)];
hgexport(fig, outputPath, hgexport('factorystyle'), 'Format', 'eps');

samebest = bestd(:, 1) == bestp(:, 1);

disp(sum(samebest) / length(itemList))
disp(sum(bestd(:, 3)) / length(itemList))
disp(sum(bestp(:, 3)) / length(itemList))