function [rawFasta] = load_fastas_from_folder(path)

    rawFasta = [];
    listing = dir(fullfile(path, '*.f*'));
    for i=1:length(listing)
        fastaFileName = fullfile(path, listing(i).name);
        rawFasta = [rawFasta; fastaread(fastaFileName)];
    end
end

