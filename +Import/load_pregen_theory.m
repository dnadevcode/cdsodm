function [barcodes, dots, rawDots, dotbars, name, lengthsBp] = load_pregen_theory(barcodeFile, rawFasta, consensus, sets)

% import Microscopy.Simulate.Core.apply_point_spread_function

indexedNames = barcodeFile.indexedNames;

lengthFracThreshold = 1 + sets.reference.lengthDiffMax;
%     if sets.reference.isCircular
lengthMin = min(consensus.lengthPx/lengthFracThreshold);
lengthMax = max(consensus.lengthPx*lengthFracThreshold);
%     else
%         lengthMin = max(estimatedBpLength);
%         lengthMax = inf;
%     end

lengthsBp = arrayfun(@(x) length(x.Sequence), rawFasta);
lengthsPx = round(lengthsBp*sets.lscale.meanbpnm/sets.camera.pxwnm);

lengthsIds = lengthsPx >= lengthMin & lengthsPx <= lengthMax;
rawFasta = rawFasta(lengthsIds);
lengthsBp = lengthsBp(lengthsIds);
seqNames = arrayfun(@(x) x.Header, rawFasta, 'un', 0);
indexedBarcodes = barcodeFile.indexedBarcodes(ismember(indexedNames, seqNames));
fastaIds = ismember(seqNames, indexedNames);
rawFasta = rawFasta(fastaIds);
lengthsBp = lengthsBp(fastaIds);

barcodes = cell(length(rawFasta), 1);
dots = cell(length(rawFasta), size(consensus.enzymes, 1));
rawDots = cell(length(rawFasta), size(consensus.enzymes, 1));
dotbars = cell(length(rawFasta), size(consensus.enzymes, 1));
name = cell(length(rawFasta), 1);

for i=1:length(rawFasta)
    ntSeq = rawFasta(i).Sequence;
    name{i} = seqNames{i};
    
    lengthBp = length(ntSeq);
    
    % CB barcode
    rawBarcode = indexedBarcodes{i};
    
    %         if sets.reference.isCircular
    oldBpPx = lengthBp/(length(rawBarcode)*lengthFracThreshold);
    oldPsfBp = sets.camera.psfnm/sets.camera.pxwnm*oldBpPx;
    newBpPx = lengthBp/consensus.lengthPx;
    newPsfBp = sets.camera.psfnm/sets.camera.pxwnm*newBpPx;
    psfDiff = (newPsfBp-oldPsfBp)/(sets.camera.pxwnm/sets.lscale.meanbpnm);
    
    if length(rawBarcode) > 2 && consensus.lengthPx > 2
        v = linspace(1, length(rawBarcode), consensus.lengthPx);
        intrpBarcode = interp1(rawBarcode, v);
        if 5*psfDiff > 1
            filterWidth = round(6*psfDiff+1);
            filterWidth = filterWidth + not(mod(filterWidth, 2));
            barcodes{i} = imgaussfilt(intrpBarcode(:)', [1 psfDiff], 'filtersize', [1 filterWidth], 'padding', 'circular');
%             barcodes{i} = apply_point_spread_function(intrpBarcode, psfDiff, 0);
        else
            barcodes{i} = intrpBarcode;
        end
    else
        barcodes{i} = rawBarcode;
    end
    
    % Enzyme recognition sites
    if isempty(consensus.enzymes); dots{i} = []; continue; end
    enzymes = consensus.enzymes;
    lenPx = consensus.lengthPx;
    cutStd = sets.emulation.cutLocationStd;
    for j=1:length(enzymes)
        [~, recSitePos] = restrict(ntSeq, enzymes{j});
        
        if length(recSitePos) > 1
          rawDots{i,j} = recSitePos(2:end);
          dots{i,j} = recSitePos(2:end)'/lengthBp*lenPx;
        else
          rawDots{i,j} = [];
          dots{i,j} = [];
        end
        
%         % Cluster sites within specified dot-precision (Could probably be simplified somewhat)
%         dots{i,j} = Common.cluster_points_min_distance(dots{i,j}, ...
%             consensus.lengthPx, ...
%             sets.bab.binWidth, ...
%             1);
        
        dotbars{i,j} = Common.gen_dotbar(dots{i,j}, ...
            cutStd, ...
            lenPx);
    end
end

end

