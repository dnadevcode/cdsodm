function longDots = shuffle_region_diffs(referenceDots, rawLengthBp, sampleLength, numrand)

  lengthLong = sum(rawLengthBp);
  allDots = arrayfun(@(n) cellfun(@(x) x + sum(rawLengthBp(1:n - 1)), ...
    referenceDots(n, :), 'un', 0), 1:length(rawLengthBp), 'un', 0);
  allDots = vertcat(allDots{:});

  numEnzymes = size(referenceDots, 2);
  longDots = cell(1, numEnzymes);

  for j = 1:numEnzymes
    thisDots = vertcat(allDots{:, j});
    backDots = sort(lengthLong - thisDots);
    randPos = datasample(0:lengthLong - sampleLength, numrand);
    sampleDots = [arrayfun(@(n) thisDots(thisDots >= randPos(n) ...
                  & thisDots < randPos(n) + sampleLength) - randPos(n) + sampleLength * (n - 1), ...
                  1:ceil(numrand / 2), 'un', 0) ...
                  arrayfun(@(n) backDots(backDots >= randPos(n) ...
                  & backDots < randPos(n) + sampleLength) - randPos(n) + sampleLength * (n - 1), ...
                  ceil(numrand / 2) + 1:numrand, 'un', 0)];
    longDots{j} = vertcat(sampleDots{:});
  end

end
