function p = trunc_normal_ev_cdf(x, m, s, n, a, b, extraPrecision)
if nargin < 5
  a = 0;
end
if nargin < 6
  b = 1;
end

eta = (x - m)./s;
alph = (a - m)./s;
bet = (b - m)./s;

ncdf = @(x) .5*(1+erf(x/sqrt(2)));
vpa_ncdf = @(x) .5*(vpa(2)-erfc(x/sqrt(2)));

if nargin < 7 || ~extraPrecision
  alph2 = ncdf(alph);
  bet2 = ncdf(bet);
  p = ((ncdf(eta) - alph2)./(bet2 - alph2)).^n;
else
  alph3 = vpa_ncdf(alph);
  bet3 = vpa_ncdf(bet);
  p = ((vpa_ncdf(eta) - alph3) ./ (bet3 - alph3)).^n;
  p = min(p, vpa(1)-10^-digits);
end