function scores = compute_random_barcode_scores(consensusBarcode, ...
    randomBarcodes)

  import SignalRegistration.unmasked_pcc_corr

  numRandom = size(randomBarcodes, 1);
  tmpBitmask = true(1, length(consensusBarcode));

  scores = nan(numRandom, 1);

  parfor i = 1:numRandom
    xcorrs = unmasked_pcc_corr(consensusBarcode, ...
      squeeze(randomBarcodes(i, :)), ...
      tmpBitmask);
    scores(i) = max(xcorrs(:));
  end

end
