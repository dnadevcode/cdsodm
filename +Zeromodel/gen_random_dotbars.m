function dotbars = gen_random_dotbars(rawDots, rawLengthBp, lengthBar, sets, sigma)

rng(1)

if nargin < 4
    sigma = sets.camera.psfnm/sets.camera.pxwnm;
end
bppx = sets.camera.pxwnm/sets.lscale.meanbpnm;
numRand = sets.zeromodel.numrand;
lengthBarBp = lengthBar*bppx;

sampleLength = sets.zeromodel.samplewidth;
lengthLong = mean(rawLengthBp)*numRand;

longDots = Zeromodel.shuffle_region_diffs(rawDots, rawLengthBp, sampleLength, round(lengthLong/sampleLength));

if all(cellfun(@isempty, longDots))
  dotbars = [];
  return
else
  dotbars = zeros(numRand, length(longDots), lengthBar);
end

for j=1:length(longDots)
    thisDots = longDots{j};
    parfor i=1:numRand
        randPosBp = rand*(lengthLong-lengthBarBp);
        randDotsBp = thisDots(thisDots >= randPosBp & ...
            thisDots <= randPosBp+lengthBarBp-1)-randPosBp;
        randDotsPx = randDotsBp/lengthBarBp*lengthBar;
%         randDotsPx = Common.cluster_points_min_distance(randDotsPx, ...
%             lengthBar, ...
%             sets.bab.binWidth, ...
%             1);
        dotbars(i, j, :) = Common.gen_dotbar(randDotsPx, sigma, lengthBar);
    end
end
end

