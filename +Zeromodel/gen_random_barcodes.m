function barcodes = gen_random_barcodes(lengthPx, numRand, sigma, pathFFT)
  % gen_random_barcodes
  % Generates the random barcodes that are used to
  % evaluate the statistical significance of match-scores.

  if nargin < 4
    disp('Please select .mat containing mean CB-barcode FFT amplitudes')
    [pathFFTFile, pathFFTFolder] = uigetfile('*.mat', ...
      'Select .mat containing mean CB-barcode FFT amplitudes');
    pathFFT = fullfile(pathFFTFolder, pathFFTFile);
  end

  if strcmp(pathFFT, fullfile(0, 0))
    disp('No mean FFT amplitudes selected, defaulting to normal distribution of amplitudes')
%     import Microscopy.Simulate.Core.apply_point_spread_function
    barcodes = normrnd(0, 1, numRand, lengthPx);

    for i = 1:numRand
      filterWidth = round(6*sigma+1);
      filterWidth = filterWidth + not(mod(filterWidth, 2));
      barcodes(i, :) = imgaussfilt(barcodes(i, :), [1 sigma], 'filtersize', [1 filterWidth], 'padding', 'circular');
%       barcodes(i, :) = apply_point_spread_function(barcodes(i, :), sigma, 0);
    end

  else
    import CBT.RandBarcodeGen.PhaseRandomization.generate_rand_barcodes_from_fft_zero_model
    meanFFTFile = load(pathFFT);

    try
      barcodes = generate_rand_barcodes_from_fft_zero_model(meanFFTFile.meanFFTEst, ...
        numRand, ...
        lengthPx);
    catch
      barcodes = generate_rand_barcodes_from_fft_zero_model(meanFFTFile.meanFFT, ...
        numRand, ...
        lengthPx);
    end

    barcodes = cell2mat(barcodes);
  end

end
