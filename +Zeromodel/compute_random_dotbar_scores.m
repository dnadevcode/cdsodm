function scores = compute_random_dotbar_scores(consensusDotbars, ...
  randomDotbars)

numRandom = size(randomDotbars, 1);
numEnzymes = length(consensusDotbars);

if isempty(consensusDotbars)
  scores = [];
else
  scores = nan(numRandom, numEnzymes, 2 * length(consensusDotbars{1}));
end

for j = 1:numEnzymes
  thisDotbar = consensusDotbars{j};
  parfor i = 1:numRandom
    
    dcorrs = Common.compute_dot_cc(thisDotbar, ...
      squeeze(randomDotbars(i, j, :)));
    scores(i, j, :) = [dcorrs(1, :) dcorrs(2, :)];
  end
end

end
