function [pvals, betaEVParams] = compute_pvals_dense(consensusBarcode, ...
    scoreMat, ...
    randomBarcodes)

  import SignalRegistration.unmasked_pcc_corr

  numRandom = size(randomBarcodes, 1);

  pvals = cell(1, length(scoreMat));

  consensusLengthPx = length(consensusBarcode);
  tmpBitmask = true(1, consensusLengthPx);

  allPcc = nan(numRandom, 2 * consensusLengthPx);
  bestPcc = nan(numRandom, 1);

  for i = 1:numRandom
    xcorrs = unmasked_pcc_corr(consensusBarcode, ...
      squeeze(randomBarcodes(i, :)), ...
      tmpBitmask);
    allPcc(i, :) = xcorrs(:)';
    bestPcc(i) = max(xcorrs(:));
  end
  
  assignin('base', 'allPcc', allPcc);

  % [thisEVParams(1), ~, thisEVParams(2)] = Zeromodel.beta_ev_fit(bestPcc(not(isnan(bestPcc))), [4 1e-16 1], [inf inf inf], [4 1 1], [false true false]);
  thisEVParams = Zeromodel.beta_ev_params(bestPcc(not(isnan(bestPcc))), consensusLengthPx);
  betaEVParams = thisEVParams;

  for k = 1:length(scoreMat)
    bNonNegativeScores = scoreMat{k} > 0;
    pvals{k} = ones(size(scoreMat{k}));
    pvals{k}(bNonNegativeScores) = 1 - Zeromodel.beta_ev_cdf(scoreMat{k}(bNonNegativeScores), thisEVParams(1), 1, thisEVParams(2));

    if any(pvals{k} == 0)
      pvals{k}(bNonNegativeScores) = vpa(1) - Zeromodel.beta_ev_cdf(scoreMat{k}(bNonNegativeScores), ...
        thisEVParams(1), ...
        1, ...
        thisEVParams(2), ...
        true);
    end

  end

  % figure;
  % histogram(allPcc)
  % figure;
  % histogram(bestPcc)
