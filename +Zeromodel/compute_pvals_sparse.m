function [pvals, normEVParams] = compute_pvals_sparse(consensusDotbars, ...
    referenceDotbars, ...
    scoreMat, ...
    randomDotbars)

  numRandom = size(randomDotbars, 1);
  numEnzymes = length(consensusDotbars);

  pvals = cell(1, size(scoreMat, 1));

  randCC = nan(numRandom, numEnzymes, 2 * length(consensusDotbars{1}));

  for i = 1:numRandom

    for j = 1:numEnzymes
      dcorrs = Common.compute_dot_cc(consensusDotbars{j}, ...
        squeeze(randomDotbars(i, j, :)));
      randCC(i, j, :) = [dcorrs(1, :) dcorrs(2, :)];
    end

  end
  numDotsExp = sum(cellfun(@(x) sum(x), consensusDotbars));
  numDotsRef = sum(cellfun(@(x) sum(x), referenceDotbars), 2);
  numDotsRnd = sum(sum(randomDotbars, 3), 2);
  sumDotsExp = sum(cellfun(@(x) sum(x.^2), consensusDotbars));
  sumDotsRef = sum(cellfun(@(x) sum(x.^2), referenceDotbars), 2);
  sumDotsRnd = sum(sum(randomDotbars.^2, 3), 2);
  sumDotsExp(numDotsExp < 2) = nan;
  sumDotsRef(numDotsRef < 2) = nan;
  sumDotsRnd(numDotsRnd < 2) = nan;

  sumCC = nansum(randCC, 2);
  normCC = sumCC ./ sqrt(sumDotsRnd * sumDotsExp);
  normCC(normCC < 1e-3) = nan;
  bestCC = squeeze(nanmax(normCC, [], 3));
  
  assignin('base', 'allDcc', normCC);

%   figure;
%   histogram(normCC, 60);

  m = nanmean(normCC(:));
  s = nanstd(normCC(:));
  [m_fit, ~, s_fit, n_fit] = Zeromodel.trunc_normal_ev_fit(bestCC(not(isnan(bestCC))), 0, 1, [0 1e-16 1e-16 1], [1 1e16 1 1e16], [m 1e-16 s 1], [false true false false], 1);
  normEVParams = [m_fit, s_fit, n_fit];

%   xr = 0:max(bestCC) / 1000:max(bestCC) * 1.2;
%   figure;
%   h = histogram(bestCC, 60);
%   hold on
%   plot(xr, Zeromodel.trunc_normal_ev_pdf(xr, m_fit, s_fit, n_fit) * h.BinWidth * sum(h.Values), 'Linewidth', 2)
%   hold off

  for k = 1:size(scoreMat, 1)
    sumScore = nansum(vertcat(scoreMat{k, :}));
    normScore = sumScore / sqrt(sumDotsExp * sumDotsRef(k));
    pvals{k} = nan(size(scoreMat{k}));
    pvals{k} = 1 - Zeromodel.trunc_normal_ev_cdf(normScore, m_fit, s_fit, n_fit, 0, 1);

    if any(pvals{k} == 0)
      pvals{k} = vpa(1) - Zeromodel.trunc_normal_ev_cdf(normScore, m_fit, s_fit, n_fit, 0, 1, true);
    end
    pvals{k} = double(pvals{k});
    pvals{k}(abs(imag(pvals{k})) > 0) = nan;
%     if any(abs(imag(pvals{k}(:)))>0); disp(min(sumScore)); disp(max(sumScore)); break; end

  end