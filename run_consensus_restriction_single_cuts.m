%% run_consensus_restriction_single_cuts
% Script for generating consensus of circular DNA that has been cut once by
% one or more restriction enzymes

rng(1)

sets = ini2struct('default_settings.ini');

paths = {'D:\Arkivet\DNAdev\Plasmid paper\data\130 kb plasmid FW group\DataSet 1a_3 cut full fragment kymos';
  'D:\Arkivet\DNAdev\Plasmid paper\data\130 kb plasmid FW group\DataSet 2a_5 cut full fragment kymos\DataSet 2a_5 cut full fragments_sorted'};
enzymes = {'AscI';
  'PmeI'};

%% Example pre-gen consensus
disp('Pre-gen consensus')

consensus = Consensus.consensus_single_cuts(paths, enzymes, sets);

[pathConsensusFile, pathConsensusFolder] = uiputfile('*.mat', 'Choose location for saving consensus', 'consensus.mat');
save(fullfile(pathConsensusFolder, pathConsensusFile), 'consensus', '-v7.3');