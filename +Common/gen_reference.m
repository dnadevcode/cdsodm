function [referenceBarcode, ...
    referenceDots, ...
    referenceRawDots, ...
    referenceDotbar, ...
    refLengthBp, ...
    name] = gen_reference(referenceListing, ...
    consensusLengthPx, ...
    consensusDotsStd, ...
    enzymes, ...
    sets)

model = Common.cb_model();

estimatedBpLength = consensusLengthPx/sets.lscale.meanbpnm*sets.camera.pxwnm;
lengthFracThreshold = 1 + sets.reference.lengthDiffMax;
%     if sets.reference.isCircular
lengthMin = min(estimatedBpLength/lengthFracThreshold);
lengthMax = max(estimatedBpLength*lengthFracThreshold);
%     else
%         lengthMin = max(estimatedBpLength);
%         lengthMax = inf;
%     end
estLengthsBp = arrayfun(@(x) x.bytes*(1-1/70), referenceListing);

referenceListing = referenceListing(estLengthsBp >= lengthMin & estLengthsBp <= lengthMax);

referenceBarcode = cell(length(referenceListing), 1);
referenceDots = cell(length(referenceListing), length(enzymes));
referenceRawDots = cell(length(referenceListing), length(enzymes));
referenceDotbar = cell(length(referenceListing), length(enzymes));
refLengthBp = nan(1, length(referenceListing));
name = cell(length(referenceListing), 1);

for i=1:length(referenceListing)
    
    [name{i}, ntSeq] = fastaread(fullfile(referenceListing(i).folder, referenceListing(i).name));
    
    % CB binding
    prob = Common.cb_theory(ntSeq, ...
        sets.freeconc.concN, ...
        sets.freeconc.concY, ...
        model.yoyoBindingConstant, ...
        model.netropsinBindingConstant, ...
        0, ...
        1);
    
    % CB barcode
    referenceBarcode{i} = Common.prob_to_pix(prob, ...
        sets.camera.psfnm/sets.lscale.meanbpnm, ...
        sets.camera.pxwnm/sets.lscale.meanbpnm, ...
        1, ...
        consensusLengthPx);
    
    % Length
    refLengthBp(i) = length(ntSeq);
    
    % Enzyme recognition sites
    if isempty(enzymes); referenceDots{i,1} = []; continue; end
    for j=1:length(enzymes)
        [~, recSitePos] = restrict(ntSeq, enzymes{j});
        recSitePos(1) = [];
        referenceRawDots{i,j} = recSitePos;
        referenceDots{i,j} = recSitePos'/length(ntSeq)*consensusLengthPx;
        
%         % Cluster sites within specified dot-precision (Could probably be simplified somewhat)
%         referenceDots{i,j} = Common.cluster_points_min_distance(referenceDots{i,j}, ...
%             consensusLengthPx, ...
%             sets.bab.binWidth, ...
%             1);
        
        referenceDotbar{i,j} = Common.gen_dotbar(referenceDots{i,j}, consensusDotsStd, consensusLengthPx);
    end
    
    fprintf('Generated reference %d out of %d\n', i, length(referenceListing))
end

end

