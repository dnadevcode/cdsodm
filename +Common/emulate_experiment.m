function [barcode, dots, dotbars, lengthPx] = emulate_experiment(sequence, enzymes, sets)

    %%

    model = Common.cb_model();
    prob = Common.cb_theory(sequence, ...
        sets.freeconc.concN, ...
        sets.freeconc.concY, ...
        model.yoyoBindingConstant, ...
        model.netropsinBindingConstant, ...
        0, 1);
                      
    lengthPx = round(length(sequence)/(sets.camera.pxwnm/sets.lscale.meanbpnm));
                                   
    rawBarcode = Common.prob_to_pix(prob, ...
        sets.camera.psfnm/sets.lscale.meanbpnm, ...
        sets.camera.pxwnm/sets.lscale.meanbpnm, ...
        1, ...
        lengthPx);
                                
    filterSize = sets.camera.psfnm/sets.camera.pxwnm;
    filterWidth = round(6*filterSize+1);
    filterWidth = filterWidth + not(mod(filterWidth, 2));
    noiseFraction = sets.emulation.noiseFraction;
    
%     testScores = zeros(1, 10000);
%     
%     for i=1:10000
      rawNoise = randn(1,lengthPx);
      blurredNoise = imgaussfilt(rawNoise(:)', [1 filterSize], 'filtersize', [1 filterWidth]);
      blurredNoise = blurredNoise+mean(rawBarcode);
      barcode = (1-noiseFraction)*rawBarcode + noiseFraction*blurredNoise;
      
%       xcorrs = SignalRegistration.unmasked_pcc_corr(barcode, rawBarcode, true(size(barcode)));
%       
%       testScores(i) = max(xcorrs(:));
%     end
%     
%     mean(testScores)
%     return
        
        
    %%
    
%     numCuts = sets.emulation.numberOfCuts;
%     randomFraction = sets.emulation.randomCutProportion;
%     numRealCuts = floor((1-randomFraction)*numCuts);
%     numFakeCuts = ceil(randomFraction*numCuts);
    stdCuts = sets.emulation.cutLocationStd;
    labelProb = sets.emulation.labelProbability;
    numFakeLabels = round(length(sequence)/1e5);
    
    pxbp = sets.lscale.meanbpnm/sets.camera.pxwnm;
    
    dots = cell(1, length(enzymes));
    dotbars = cell(1, length(enzymes));
    if lengthPx < 2; return; end
    for i=1:length(enzymes)
        [~, recSitesBp] = restrict(sequence, enzymes{i});
        recSitesBp(1) = [];
        recSitesPx = recSitesBp*pxbp;
        
%         if isempty(recSitesPx)
%             randomCuts = rand(numCuts, 1)*lengthPx;
%         else
%             randomCuts = recSitesPx(randi(length(recSitesPx), numRealCuts, 1));
%             randomCuts = randomCuts + normrnd(0, stdCuts, numRealCuts, 1);
%             randomCuts = mod(randomCuts + lengthPx, lengthPx);
%             randomCuts = [randomCuts; rand(numFakeCuts, 1)*lengthPx];
%         end
%         dots{i} = sort(Consensus.gen_cut_clusters_experiment(randomCuts, lengthPx, sets));
%         dotbars{i} = Common.gen_dotbar(dots{i}, stdCuts, lengthPx);

        numRealLabels = round(labelProb * length(recSitesPx));
        realLabels = datasample(recSitesPx, numRealLabels, 'replace', false);
        fakeLabels = rand(1, numFakeLabels) * lengthPx;
        dots{i} = sort([realLabels(:)' fakeLabels(:)']);
        thisDotbar = Common.gen_dotbar(dots{i}, stdCuts, lengthPx);
        rawNoise = randn(1,lengthPx);
        blurredNoise = imgaussfilt(rawNoise(:)', [1 filterSize], 'filtersize', [1 filterWidth]);
        blurredNoise = blurredNoise+mean(thisDotbar);
        dotbars{i} = (1-noiseFraction)*thisDotbar + noiseFraction*blurredNoise;
        
    end

end

