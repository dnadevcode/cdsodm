function zVals = compute_zvals(pDense, pSparse, w)
  % compute_zvals
  
  if isempty(pSparse)
    switch w
      case 1
        zVals = cellfun(@(x) nan(size(x)), pDense, 'un', 0);
      otherwise
        zVals = cellfun(@(x) double(norminv(x)), pDense, 'un', 0);
    end
    return
  end

  zVals = cell(size(pDense));

  for i = 1:length(zVals)
    invDense = double(norminv(pDense{i}));
    if isempty(pSparse{i})
      if w == 0
        zVals{i} = nan(size(pDense{i}));
      else
        zVals{i} = invDense;
      end
      continue
    end
    invSparse = double(norminv(pSparse{i}));
    switch w
      case 1
        zVals{i} = invDense;
      case 0
        zVals{i} = invSparse;
      otherwise
%         zVals{i} = nanmin(invDense, invSparse);
        isSparseCompared = not(isnan(invSparse));
        zVals{i}(isSparseCompared) = nansum(vertcat(w .* invDense(isSparseCompared), (1 - w) .* invSparse(isSparseCompared))) ./ sqrt(nansum(vertcat(w.^2, (1 - w).^2)));
        zVals{i}(not(isSparseCompared)) = invDense(not(isSparseCompared));
    end
  end

end
