function [winnerIds, zValsBest, bestShift, zSigma] = get_winners(pvalsDense, ...
    pvalsSparse, ...
    pvalsDenseResampled, ...
    pvalsSparseResampled, ...
    omega, ...
    sets)

  zVals = Common.compute_zvals(pvalsDense, ...
    pvalsSparse, ...
    omega);

  if isempty(zVals)
    winnerIds = [];
    zValsBest = [];
    bestShift = [];
    zSigma = nan;
    return
  end

  zValsBest = nan(length(zVals), 1);
  bestShift = nan(length(zVals), 1);

  for i = 1:length(zVals)
    [zValsBest(i), bestShift(i)] = min(zVals{i}, [], 2);
  end
  
%   figure;histogram(zValsBest)

  thresholdPassed = normcdf(zValsBest) <= sets.output.pthreshold;

  if (isempty(pvalsDenseResampled) && isempty(pvalsSparseResampled)) || (all(isnan(pvalsDenseResampled(:))) && all(isnan(pvalsSparseResampled(:))))
    winnerIds = find(thresholdPassed);
    zSigma = nan;
  else
    resampledZvals = Common.compute_zvals({pvalsDenseResampled}, ...
      {pvalsSparseResampled}, ...
      omega);
    resampledZvals = resampledZvals{1};
%     resampledZvals = nanmin(double(norminv(pvalsDenseResampled)), double(norminv(pvalsSparseResampled)));
    
%     figure;histogram(resampledZvals, 60)

    badIds = isinf(resampledZvals) | isnan(resampledZvals);
    zSigma = nanstd(resampledZvals(~badIds));

    resamplingPassed = zValsBest <= min(zValsBest(:)) + sets.output.numSigma * zSigma;

    if any(isnan(resamplingPassed)) || any(isinf(resamplingPassed))
      throw(MException('Resampling:BadBoolArray', 'Something wrong with Z-sigma evaluation'))
    end

    winnerIds = find(thresholdPassed .* resamplingPassed);
  end

end
