function [concY, concN] = compute_free_conc(pathLambda, y0, n0, cDNA)
  % compute_free_conc
  % Computing free concentrations
  %     Args:
  %
  %     Returns:
  %         sets: Output settings
  %

  [~, lambdaSequence] = fastaread(pathLambda);

  rs = lambdaSequence;
  model = Common.cb_model();
  probsBinding1 = @(x) Common.cb_theory(rs, x(2), x(1), model.yoyoBindingConstant, ...
    model.netropsinBindingConstant, 0, 1);
  probsBinding2 = @(x) Common.cb_theory(rs, x(2), x(1), model.yoyoBindingConstant, ...
    model.netropsinBindingConstant, 0, 2);

  x0 = [y0, n0];

  fun = @(x) x0 - x - [mean(probsBinding1(x)) ...
                  mean(probsBinding2(x))] * cDNA * 0.25;

  % we minimise the sum square
  fun2 = @(x) sum(fun(x).^2);

  % using fminsearch
  [xNew] = fminsearch(fun2, x0);

  concY = xNew(1);
  concN = xNew(2);

end
