function dcorrs = compute_dot_cc(barA, barB)

barA(barA < 10^-floor(digits/2-1)) = 0;
barB(barB < 10^-floor(digits/2-1)) = 0;

dcorrF = ifft(fft(barB(:)').*conj(fft(barA(:)', length(barB))));
dcorrB = ifft(fft(barB(:)').*conj(fft(fliplr(barA(:)'), length(barB))));

dcorrs = [dcorrF; dcorrB];

dcorrs(abs(imag(dcorrs)) > 0 | dcorrs < 0) = nan;

