function [resampledPvalsDense, resampledPvalsSparse] = evaluate_best_matches(consensusBarcode, ...
    consensusDotbars, ...
    consensusLengthPx, ...
    consensusEnzymes, ...
    pvalsDense, ...
    pvalsSparse, ...
    referenceBarcodes, ...
    referenceDotbars, ...
    betaEVParams, ...
    dotsEVParams, ...
    omega, ...
    sets)

  import SignalRegistration.unmasked_pcc_corr

  numEnzymes = length(consensusEnzymes);

  zVals = Common.compute_zvals(pvalsDense, ...
    pvalsSparse, ...
    omega);

  optZVals = nan(1, length(zVals));
  optShift = nan(1, length(zVals));

  %     if all(cellfun(@(x) all(isnan(x)), zVals))
  %         resampledPvalsDense = nan(1, sets.resampling.numReSample);
  %         resampledPvalsSparse = nan(1, sets.resampling.numReSample);
  %         return
  %     end

  % Find optimal shift for each reference
  for r = 1:length(zVals)
    [optZVals(r), optShift(r)] = nanmin(zVals{r});
  end

  % Find best reference
  [bestZ, bestRefId] = nanmin(optZVals);

  if isinf(bestZ)
    disp(pvalsDense{bestRefId})
    disp(pvalsSparse{bestRefId})
    throw(MException('Resampling:BestMatch', 'Best match had zero p-val'))
  end

  if normcdf(bestZ) > sets.output.pthreshold
    resampledPvalsDense = [];
    resampledPvalsSparse = [];
    return
  end

  % Align reference to consensus
  tmpShift = mod(optShift(bestRefId) - 1, consensusLengthPx) + 1;
  refDense = circshift(referenceBarcodes{bestRefId}, 1 - tmpShift);
  refSparse = cellfun(@(x) circshift(x, 1 - tmpShift), ...
    referenceDotbars(bestRefId, :), 'un', 0);

  if optShift(bestRefId) > consensusLengthPx
    consensusBarcode = fliplr(consensusBarcode);
    consensusDotbars = cellfun(@(x) fliplr(x), consensusDotbars, 'un', 0);
  end

  alignedDense = [zscore(consensusBarcode); zscore(refDense)];
  alignedSparse = [consensusDotbars refSparse];

  % Generate resampled barcodes
  [resampledDense, ...
      resampledSparse] = Common.resample_aligned_barcodes(alignedDense, ...
    alignedSparse, ...
    sets);
  resampledConDense = resampledDense(:, 1)';
  resampledRefDense = resampledDense(:, 2)';
  resampledConSparse = resampledSparse(:, 1:length(consensusDotbars));
  resampledRefSparse = resampledSparse(:, length(consensusDotbars) + 1:end);

  % Calculate resampled match-scores
  resampledDenseScores = nan(1, sets.resampling.numReSample);
  resampledSparseScores = nan(numEnzymes, sets.resampling.numReSample);

  if omega > 0
    tmpBitmask = true(1, length(resampledConDense{1}));

    for s = 1:sets.resampling.numReSample
      tmpScore = unmasked_pcc_corr(resampledConDense{s}, resampledRefDense{s}, tmpBitmask);
      resampledDenseScores(s) = tmpScore(1);
    end

  end

  if omega < 1

    for s = 1:sets.resampling.numReSample

      for j = 1:numEnzymes
        tmpScore = Common.compute_dot_cc(resampledConSparse{s, j}, resampledRefSparse{s, j});
        resampledSparseScores(j, s) = tmpScore(1);
      end

    end

  end
  
  numDotsCon = zeros(1, sets.resampling.numReSample);
  numDotsRef = zeros(1, sets.resampling.numReSample);
  sumDotsCon = zeros(1, sets.resampling.numReSample);
  sumDotsRef = zeros(1, sets.resampling.numReSample);

  for i = 1:numEnzymes
    numDotsCon = numDotsCon + cellfun(@(x) sum(x), resampledConSparse(:, i))';
    numDotsRef = numDotsRef + cellfun(@(x) sum(x), resampledRefSparse(:, i))';
    sumDotsCon = sumDotsCon + cellfun(@(x) sum(x.^2), resampledConSparse(:, i))';
    sumDotsRef = sumDotsRef + cellfun(@(x) sum(x.^2), resampledRefSparse(:, i))';
  end

  sumDotsCon(numDotsCon < 2) = nan;
  sumDotsRef(numDotsRef < 2) = nan;
  resampledSparseScores = nansum(resampledSparseScores, 1) ./ sqrt(sumDotsCon .* sumDotsRef);
  % resampledSparseScores(resampledSparseScores == 0) = nan;

  %     figure;
  %     histogram(resampledDenseScores, 60)

%       figure;
%       h = histogram(resampledSparseScores, 60);
%       hold on
%       plot(0:1/1000:1, ...
%       Zeromodel.trunc_normal_ev_pdf(0:1/1000:1, dotsEVParams(1), dotsEVParams(2), dotsEVParams(3))*h.BinWidth*sum(h.Values), 'Linewidth', 2)
%       hold off

  % Calculate resampled p-values
  resampledPvalsDense = nan(size(resampledDenseScores));
  resampledPvalsSparse = nan(size(resampledSparseScores));

  if omega > 0
    resampledPvalsDense = 1 - Zeromodel.beta_ev_cdf(resampledDenseScores, ...
      betaEVParams(1), ...
      1, ...
      betaEVParams(2));

    doExtraPrecision = resampledPvalsDense == 0;
    resampledPvalsDense(doExtraPrecision) = vpa(1) - Zeromodel.beta_ev_cdf(resampledDenseScores(doExtraPrecision), ...
      betaEVParams(1), ...
      1, ...
      betaEVParams(2), ...
      true);
    
    resampledPvalsDense = double(resampledPvalsDense);
    resampledPvalsDense = real(resampledPvalsDense);
  end

  if omega < 1
    resampledPvalsSparse = 1 - Zeromodel.trunc_normal_ev_cdf(resampledSparseScores, ...
      dotsEVParams(1), ...
      dotsEVParams(2), ...
      dotsEVParams(3), ...
      0, ...
      1);

    doExtraPrecision = resampledPvalsSparse == 0;
    resampledPvalsSparse(doExtraPrecision) = vpa(1) - Zeromodel.trunc_normal_ev_cdf(resampledSparseScores(doExtraPrecision), ...
      dotsEVParams(1), ...
      dotsEVParams(2), ...
      dotsEVParams(3), ...
      0, ...
      1, ...
      true);
    resampledPvalsSparse = double(resampledPvalsSparse);
    resampledPvalsSparse = real(resampledPvalsSparse);
  end
  
%   figure;histogram(-norminv(resampledPvalsDense), 60)
%   figure;histogram(-norminv(resampledPvalsSparse), 60)

end
