function dotbar = gen_dotbar(dots, dotStd, barLength)

dotbar = zeros(1, barLength);
dots(dots == 0) = barLength;
dotbar(min(ceil(dots), barLength)) = 1;
filterWidth = round(6*dotStd+1);
filterWidth = filterWidth + not(mod(filterWidth, 2));
dotbar = imgaussfilt(dotbar(:)', [1 dotStd], 'filtersize', [1 filterWidth], 'padding', 'circular');
% dotbar = apply_point_spread_function(dotbar, dotStd, 0);
dotbar(dotbar < 1e-3) = 0;

end

