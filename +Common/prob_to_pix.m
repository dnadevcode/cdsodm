function barcodePxRes = prob_to_pix(prob, ...
                                    psfbp, ...
                                    pxInBp, ...
                                    isCircular, ...
                                    lengthPx)

    % interpolate barcode in bp-res to the experimental estimate
    if nargin > 4
        lengthBp = lengthPx*pxInBp;
        v = linspace(1, length(prob), lengthBp);
        prob = interp1(prob, v)';
    end
        
    % Convolve with a Gaussian
    if length(prob) > 2*psfbp
        filterWidth = round(6*psfbp+1);
        filterWidth = filterWidth + not(mod(filterWidth, 2));
        if isCircular
          barcodeBpRes = imgaussfilt(prob(:)', [1 psfbp], 'filtersize', [1 filterWidth], 'padding', 'circular');
        else
          barcodeBpRes = imgaussfilt(prob(:)', [1 psfbp], 'filtersize', [1 filterWidth], 'padding', 0);
        end
    else
        barcodeBpRes = mean(prob)*ones(size(prob));
    end
    
    % convert to px resolution    
    nPxRaw = length(barcodeBpRes)/pxInBp;
    if isCircular
        nPx = round(nPxRaw);
    else
        nPx = floor(nPxRaw);
    end
    pxInter = nPxRaw/nPx*pxInBp;
    
    barcodePxRes(nPx) = 0;
    switch nPx > 1
        case true
            xtraseq = cat(find(size(barcodeBpRes) - 1), ...
                          barcodeBpRes(end-round(pxInter):end), ...
                          barcodeBpRes, ...
                          barcodeBpRes(1:round(pxInter)));
        case false
            xtraseq = cat(find(size(barcodeBpRes) - 1), ...
                          barcodeBpRes, ...
                          barcodeBpRes, ...
                          barcodeBpRes);
    end
    
    for i = 1:nPx
        barcodePxRes(i) = mean(xtraseq(floor(((i-.5)*pxInter)+1):floor(((i+1.5)*pxInter))));
    end

end

