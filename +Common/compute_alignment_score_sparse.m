function [score] = compute_alignment_score_sparse(consensusDotbars, ...
  referenceDotbars)

%     import SignalRegistration.unmasked_pcc_corr

numRefs = size(referenceDotbars, 1);
numEnzymes = size(consensusDotbars, 2);

score = cell(numRefs, numEnzymes);
% exscore = cell(numRefs, numEnzymes);

for j = 1:numEnzymes
  thisDotbar = consensusDotbars{j};
%   tmpBitmask = true(1, length(thisDotbar));
  parfor i = 1:numRefs
    
    refDb = referenceDotbars{i, j};
    dcorrs = Common.compute_dot_cc(thisDotbar, refDb);
%     pcorrs = unmasked_pcc_corr(thisDotbar, refDb, tmpBitmask);
    score{i, j} = [dcorrs(1, :) dcorrs(2, :)];
%     exscore{i, j} = [pcorrs(1, :) pcorrs(2, :)];
  end
  
end

end
