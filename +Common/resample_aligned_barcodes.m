function [resampledBarcodes, resampledDotbars, sampleLengthPx] = resample_aligned_barcodes(barcodes, dotbars, sets)

  blockWidth = sets.resampling.blockWidth;
  numReSample = sets.resampling.numReSample;
  lengthPx = length(barcodes(1, :));

  numBlocks = floor(lengthPx / blockWidth);
  sampleLengthPx = numBlocks * blockWidth;

  resampledBarcodes = cell(numReSample, 2);
  resampledDotbars = cell(numReSample, length(dotbars));

  for i = 1:numReSample

    % Generate blocks
    blockCoords = zeros(numBlocks, 2);
    randStartPx = randi(lengthPx);

    for j = 1:numBlocks
      blockCoords(j, 1) = mod(randStartPx + (j - 1) * blockWidth - 1, lengthPx) + 1;
      blockCoords(j, 2) = mod(blockCoords(j, 1) + blockWidth - 2, lengthPx) + 1;
    end

    % Select blocks
    selectedBlocks = datasample(1:numBlocks, numBlocks);

    % Generate resampled barcodes
    for j = 1:numBlocks

      if blockCoords(selectedBlocks(j), 2) < blockCoords(selectedBlocks(j), 1)
        blockIdx = [blockCoords(selectedBlocks(j), 1):lengthPx, 1:blockCoords(selectedBlocks(j), 2)];
      else
        blockIdx = blockCoords(selectedBlocks(j), 1):blockCoords(selectedBlocks(j), 2);
      end

      % Dense
      for k = 1:2
        resampledBarcodes{i, k} = [resampledBarcodes{i, k} barcodes(k, blockIdx)];
      end

      % Sparse
      for k = 1:length(dotbars)
        newDotbar = dotbars{k}(blockIdx);
        resampledDotbars{i, k} = [resampledDotbars{i, k} newDotbar];
      end

    end

%     if length(resampledBarcodes{i, 1}) ~= sampleLengthPx ...
%         || length(resampledBarcodes{i, 2}) ~= sampleLengthPx ...
%         || length(resampledDotbars{i, 1}) ~= sampleLengthPx ...
%         || length(resampledDotbars{i, 2}) ~= sampleLengthPx
%       throw(MException('Resampling:BadBlocks', 'Something wrong with block concatentation'))
%     end

  end

end
