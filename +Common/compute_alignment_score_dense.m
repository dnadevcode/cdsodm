function score = compute_alignment_score_dense(consensusBcs, ...
    referenceBcs)

    import SignalRegistration.unmasked_pcc_corr

    numRefs = length(referenceBcs);
        
    tmpBitmask = true(1, length(consensusBcs));
    score = cell(numRefs, 1);
    
    parfor i=1:numRefs
        refBc = referenceBcs{i};
        xcorrs = unmasked_pcc_corr(consensusBcs, refBc, tmpBitmask);
        score{i} = [xcorrs(1,:) xcorrs(2,:)];
    end

end

