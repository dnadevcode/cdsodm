%% run_gen_cb_zeromodel
% Script for generating the estimated mean FFT stuff for CB zeromodel


%%

rng(1)

sets = ini2struct('default_settings.ini');
model = Common.cb_model();
pathReference = {'D:\Arkivet\DNAdev\plasmids_FW';
  'D:\Arkivet\DNAdev\RefSeqUnpacked'};
outputFolder = 'data';

shortestLength = 5e4;
longestLength = 8e5;


%%

rawFasta = cellfun(@(x) Import.load_fastas_from_folder(x), pathReference, 'un', 0);
rawFasta = vertcat(rawFasta{:});
seqLengths = arrayfun(@(x) length(x.Sequence), rawFasta);
rawFasta = rawFasta(seqLengths >= shortestLength & seqLengths <= longestLength);

%%

zeroModelBarcodes = cell(1, length(rawFasta));

parfor i=1:length(rawFasta)
    ntSeq = rawFasta(i).Sequence;

    % CB binding
    prob = Common.cb_theory(ntSeq, ...
        sets.freeconc.concN, ...
        sets.freeconc.concY, ...
        model.yoyoBindingConstant, ...
        model.netropsinBindingConstant, ...
        0, ...
        1);

    % CB barcode
    zeroModelBarcodes{i} = zscore(Common.prob_to_pix(prob, ...
        sets.camera.psfnm/sets.lscale.meanbpnm, ...
        sets.camera.pxwnm/sets.lscale.meanbpnm, ...
        1));
    
    fprintf('Finished generating barcode %d out of %d\n', i, length(rawFasta))
end


%%
import CBT.RandBarcodeGen.PhaseRandomization.gen_mean_fft_freq_mags
meanFFT = gen_mean_fft_freq_mags(zeroModelBarcodes);

%%
save(fullfile(outputFolder, 'cb_zeromodel.mat'), 'meanFFT', '-v7.3')

