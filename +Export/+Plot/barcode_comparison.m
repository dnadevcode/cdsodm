function alignScore = barcode_comparison(exp, thr, pxPerBp, legendtext, barType, headertext)

%   dims = [1920 600];
  fs = 20;
  titH = .85;
  titW = .30;
%   paperoffset = 0.03;
%   paperspacing = 0.06;

%   fig = figure;
%   set(fig, 'Position', [[0, 0] dims]);

  if iscell(exp)
    lenExp = length(exp{1});
  else
    lenExp = length(exp);
  end

  if strcmp(barType, 'pcc')
    expMean = mean(exp);
    expStd = std(exp);

    thrMean = mean(thr);
    thrStd = std(thr);

    tmpBm = true(1, lenExp);
    tmpScore = Common.compute_pcc(thr, exp, tmpBm, tmpBm);
  elseif strcmp(barType, 'dotcc')

    if iscell(exp)
      tmpScore = arrayfun(@(i) Common.compute_dot_cc(thr{i}, exp{i}), 1:length(exp), 'un', 0);
      tmpScore = nansum(cat(3, tmpScore{:}), 3) / sqrt(sum(cellfun(@(x) sum(x.^2), thr)) * sum(cellfun(@(x) sum(x.^2), exp)));
    else
      tmpScore = Common.compute_dot_cc(thr, exp);
    end

  end

  [alignScore, pxStart] = max([tmpScore(1, :) tmpScore(2, :)]);

  if pxStart > lenExp

    if iscell(exp)
      thr = cellfun(@fliplr, thr, 'un', 0);
    else
      thr = fliplr(thr);
    end

  end

  expColor = [.6, .6, .6]; % colors(1,:);
  thrColor = 'black'; % colors(2,:);
  
  v = linspace(1, lenExp, 10 * lenExp);
  xi = .1:.1:lenExp;

  if iscell(exp)

    for i = 1:length(exp)
      plot(xi, interp1(exp{i}, v, 'pchip'), 'linewidth', 2, 'color', expColor, 'HandleVisibility', 'off')
      hold on
    end

  else
    plot(exp, 'linewidth', 2, 'color', expColor)
    hold on
  end

  b = mod(pxStart, lenExp);
  if strcmp(barType, 'pcc')
    plot(circshift((thr - thrMean) / thrStd * expStd + expMean, b - 1), 'linewidth', 2, 'color', thrColor)
    text(titW, titH, ['Optimal aligment score $\hat{C}=$' num2str(alignScore, 3)], 'Interpreter', 'latex', 'fontsize', fs, 'horizontalalignment', 'center', 'units', 'normalized');
  elseif strcmp(barType, 'dotcc')

    if iscell(thr)

      for i = 1:length(thr)
        intrpThr = interp1(circshift(thr{i}, b - 1), v, 'pchip');
        intrpExp = interp1(exp{i}, v, 'pchip');
        plot(xi, intrpThr, 'linewidth', 2, 'color', thrColor, 'HandleVisibility', 'off')
        area(xi, min(intrpThr, intrpExp), 'FaceAlpha', 0.5)
        [~, d] = findpeaks(intrpExp);
        d = d / 10;
        scatter(d, zeros(size(d)), 200, 'black', 'o', 'LineWidth', 2, 'HandleVisibility', 'off');
      end

    else
      plot(xi, interp1(circshift(thr, b - 1), v, 'pchip'), 'linewidth', 2, 'color', thrColor)
    end

    text(titW, titH, ['Optimal alignment score $\hat{D}=$' num2str(alignScore, 3)], 'Interpreter', 'latex', 'fontsize', fs, 'horizontalalignment', 'center', 'units', 'normalized');
  end

  ylabel('Rescaled intensity', 'Interpreter', 'latex', 'fontsize', fs)
  ax = gca;
  set(ax, 'fontsize', fs);
  ticks = 1:floor(20 * 1000 / pxPerBp):lenExp;
  ticksx = [0, cumsum(ones(1, length(ticks) - 1) * 20)];
  ax.XTick = ticks;
  ax.XTickLabel = ticksx;
  yticks([]);
  xlim([0 lenExp + 1])
  ylim(ylim() + [0 .5 * (max(ylim()) - min(ylim()))]);
  xlabel('Position (kbp)', 'Interpreter', 'latex', 'fontsize', fs)

  if not(isempty(legendtext))
    legend(legendtext, 'Interpreter', 'latex', 'location', 'northeast', 'fontsize', fs);
  end
  
  if not(isempty(headertext))
    tit = title(headertext, 'Interpreter', 'latex', 'fontsize', fs);
    set(tit, 'HorizontalAlignment', 'left');
    set(tit, 'Position', get(tit, 'Position').*[0 1 1]);
  end

  hold off

%   ax = gca;
%   outerpos = ax.OuterPosition;
%   ti = ax.TightInset;
%   left = outerpos(1) + ti(1);
%   bottom = outerpos(2) + ti(2) + paperoffset;
%   ax_width = outerpos(3) - ti(1) - ti(3) - 0.01;
%   ax_height = outerpos(4) - ti(2) - ti(4) - paperspacing;
%   ax.Position = [left bottom ax_width ax_height];

%   fig.PaperPositionMode = 'auto';
%   fig_pos = fig.PaperPosition;
%   fig.PaperSize = [fig_pos(3) fig_pos(4)];

end
