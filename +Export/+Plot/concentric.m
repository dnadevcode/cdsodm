function [] = concentric(fig, barcodes, sites, marks, innerW, gapW, barcodeW, ...
                          outerW, histW, bgColor, cmColor, textlabel, dolegend)
                     
%     fig = figure('Renderer', 'painters');
%     whitebg(fig, bgColor);

    fs = 20;
    
    numBarcodes = length(barcodes);

    widths = gapW*ones(numBarcodes*2, 1);
    widths(1) = innerW;
    widths(2:2:2*numBarcodes) = barcodeW;
    widths(2*numBarcodes - 1) = outerW;
    radii = cumsum([0; widths]);


    barcodeLengths = cellfun(@(x) length(x), barcodes);
    barcodeLengths = barcodeLengths(:);
    maxLen = max(barcodeLengths);
    theta = -pi:(2*pi/maxLen):pi;
    Xs = radii*cos(theta);
    Ys = radii*sin(theta);
    Cs = NaN(2*numBarcodes + 1, maxLen + 1);

    for barcodeNum=1:numBarcodes
        currBarcode = [barcodes{barcodeNum}, NaN(1, maxLen - barcodeLengths(barcodeNum))];
        Cs(barcodeNum*2, :) = [currBarcode, currBarcode(1)];
    end
    hAxis = gca;
    surf = pcolor(hAxis, Xs, Ys, Cs);
    hold on
    surf.FaceColor = 'interp';
    
    set(get(hAxis, 'ColorSpace'), 'Colormap', colormap(cmColor));
    caxis auto
    
    subPx = 10;
    maxLen2 = subPx*maxLen;
    theta2 = -pi:(2*pi/maxLen2):pi;
    
    roundedSites = mod(round(sites), maxLen);
    binnedSites = arrayfun(@(i) sum(roundedSites == mod(i, maxLen)), 0:maxLen);
    binnedSites = interp1(binnedSites, linspace(1, maxLen+1, maxLen2+1));
    binnedSites = binnedSites/max(binnedSites);
    
    hX = (radii(end)+2*outerW+histW*binnedSites).*cos(theta2);
    hY = (radii(end)+2*outerW+histW*binnedSites).*sin(theta2);
    
    roundedMarks = mod(round(subPx*(marks + .5)), maxLen2) + 1;
    sX = (radii(end)+2*outerW+histW*binnedSites(roundedMarks)).*cos(theta2(roundedMarks));
    sY = (radii(end)+2*outerW+histW*binnedSites(roundedMarks)).*sin(theta2(roundedMarks));
%     disp(binnedSites')
    
    radLim = radii(end)+2*outerW+histW*max(binnedSites);
    xlim([-radLim radLim])
    ylim([-radLim radLim])
    
    axis square;
    axis off;
%     axis tight;
    set(hAxis, 'XTickLabel', [], 'YTickLabel',[]); 
    grid off;
    shading flat; %shading interp;
    
    
    nAxis = hAxis;%axes(fig);
    hist = plot(nAxis, hX, hY, 'color', [0.2, 0.6, 0.8], 'linewidth', 2);
    
    
    set(nAxis, 'color', 'none', 'xtick', [], 'ytick', [], 'box', 'off');
    axis square;
    axis off;
%     axis tight;
    set(nAxis, 'XTickLabel', [], 'YTickLabel',[]); 
    grid off;
    xlim([-radLim radLim])
    ylim([-radLim radLim])
    
    
    sAxis = hAxis;%axes(fig);
    scat = scatter(sAxis, sX, sY, 200, 'black', 'o', 'LineWidth', 2);
    
    set(sAxis, 'color', 'none', 'xtick', [], 'ytick', [], 'box', 'off');
    axis square;
    axis off;
%     axis tight;
    set(sAxis, 'XTickLabel', [], 'YTickLabel',[]); 
    grid off;
    xlim([-radLim radLim])
    ylim([-radLim radLim])
    
    if dolegend
      legendtext{1} = 'Experimental cuts';
      legendtext{2} = 'Significant clusters';
      legend([hist; scat], legendtext, 'Interpreter', 'latex', 'FontSize', fs, 'location', 'northeast');
  %     Pos = get(Leg, 'Position');
  %     set(Leg, 'Position', [Pos(1), .88, Pos(3), Pos(4)]);
    end
    
    text(.02, .96, textlabel, 'Interpreter', 'latex', 'units', 'normalized', 'FontSize', fs);
    
end

