function [] = score_distribution(score, fitPdf, bars, xrange, xlab, ylab, headertext, legtext, leftlegend, bestScore, fs, ebPos, sigma)
    
%     dims = [1400, 1000];
        
%     fs = 36;

%     fig = figure('Renderer', 'painters');
%     set(fig, 'Position', [[0, 0] dims]);
    h = histogram(score(:), xrange(1):(xrange(end)-xrange(1))/bars:xrange(end));
    hold on
    if not(isempty(fitPdf))
      plot(xrange, fitPdf(xrange)*sum(h.Values)*h.BinWidth, 'linewidth', 4);
    end
    
%     text(bestScore, 25, 'Best score', 'interpreter', 'latex', 'FontSize', 36, 'horizontalalignment', 'center');
    
    if isempty(xlab)
        xticks([]);
    end
    if isempty(ylab)
        yticks([]);
    end
    xlabel(xlab, 'interpreter', 'latex', 'FontSize', fs);
    ylabel(ylab, 'interpreter', 'latex', 'FontSize', fs);
    set(gca, 'FontSize', fs)
    
    yl = ylim;
    ylim([0 yl(2)*1.5])
    Export.Plot.arrow([bestScore, yl(2)/4], [bestScore, 0], 'width', 6, 'length', 30, 'baseangle', 90);
    
    if nargin > 12
      for i=1:3
        errorbar(ebPos, yl(2)/3, i*sigma, 0, 'horizontal', 'o', ...
                       'linewidth', 2, 'Color', 'black', ...
                       'CapSize', 20, ...
                       'MarkerSize', 1);
%        text(ebPos - i*sigma, (2*mod(i, 2)-1)*yl(2)/10 + yl(2)/3, [num2str(i) '$\sigma$'], 'interpreter', 'latex', 'FontSize', fs, 'horizontalalignment', 'center');
      end
    end
    
    hold off
    
    xlim([xrange(1) xrange(end)])
    
    if not(isempty(legtext))
        if leftlegend
            legend(legtext, 'interpreter', 'latex', 'FontSize', fs, 'location', 'northwest');
        else
            legend(legtext, 'interpreter', 'latex', 'FontSize', fs, 'location', 'northeast');
        end
    end
    
  if not(isempty(headertext))
    tit = title(headertext, 'Interpreter', 'latex', 'fontsize', fs);
    set(tit, 'HorizontalAlignment', 'left');
    set(tit, 'units', 'normalized');
    set(tit, 'Position', get(tit, 'Position').*[0 1 1]);
  end
    
%     ax = gca;
%     outerpos = ax.OuterPosition;
%     ti = ax.TightInset; 
%     left = outerpos(1) + ti(1);
%     bottom = outerpos(2) + ti(2);
%     ax_width = outerpos(3) - ti(1) - ti(3);
%     ax_height = outerpos(4) - ti(2) - ti(4);
%     ax.Position = [left bottom ax_width ax_height];
    
%     fig.PaperPositionMode = 'auto';
%     fig_pos = fig.PaperPosition;
%     fig.PaperSize = [fig_pos(3) fig_pos(4)];
    
%     [dir, file] = Export.trim_file_and_dir_name('Output', [leg1 num2str(randi(9999999)) '.eps' ]);
%     hgexport(fig, [dir file], hgexport('factorystyle'), 'Format', 'eps');
end

