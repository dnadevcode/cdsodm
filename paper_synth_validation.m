sets = ini2struct('default_settings.ini');

outf = 'D:\Arkivet\DNAdev\Plasmid paper\figures\paper_final';
pathFFT = 'data\cb_zeromodel.mat';
pathReference = {'D:\Arkivet\DNAdev\plasmids_FW';
  'D:\Arkivet\DNAdev\RefSeqUnpacked'};

referenceid = 23;

fs = 34;

shortestLength = 5e4;
longestLength = 8e5;

pathConsensus = 'data\130kb.mat';
tmpFile = load(pathConsensus);
consensus = tmpFile.consensus;
consensus.lengthPx = length(consensus.barcode);
consensus.dotbars = arrayfun(@(i) Common.gen_dotbar(consensus.dots{i}, ...
  consensus.dotStd, consensus.lengthPx), 1:length(consensus.dots), 'un', 0);

rawFasta = cellfun(@(x) Import.load_fastas_from_folder(x), pathReference, 'un', 0);
rawFasta = vertcat(rawFasta{:});
seqLengths = arrayfun(@(x) length(x.Sequence), rawFasta);
rawFasta = rawFasta(seqLengths >= shortestLength & seqLengths <= longestLength);

reference = struct();
[reference.barcodes, ...
    reference.dots, ...
    reference.rawDots, ...
    reference.dotbars, ...
    reference.names, ...
    reference.lengthBp] = Import.load_pregen_theory(pregenBarcodes, ...
    rawFasta(referenceid), ...
    consensus, ...
    sets);
  
zDense = nan(1, 1000);

%%
for j=1:1000
  %% Emulate experiment
  consensus = struct();
  [consensus.barcode, ...
      consensus.dots, ...
      consensus.dotbars, ...
      consensus.lengthPx] = Common.emulate_experiment(rawFasta(referenceid).Sequence, ...
      enzymes, ...
      sets);
  %% Calculate match-scores
  matchScoreDense = Common.compute_alignment_score_dense(consensus.barcode, ...
    reference.barcodes);
  %% Calculate p-values
  randomBarcodes = Zeromodel.gen_random_barcodes(consensus.lengthPx, ...
      sets.zeromodel.numrand, ...
      sets.camera.psfnm/sets.camera.pxwnm, ...
      pathFFT);
  pvalsDense = Zeromodel.compute_pvals_dense(consensus.barcode, ...
    matchScoreDense, ...
    randomBarcodes);
  %% Compute Z
  zDense(j) = max(-norminv(pvalsDense{1}));
end

%% 

fig = figure('Renderer', 'painters');
dims = [1600 400];
set(fig, 'Position', [[0, 0] dims]);
ax1 = gca; %subplot(3,1,1);
Export.Plot.score_distribution(zDense(:), [], 100, 0:0.001:10, ...
  '$Z_\textrm{dense}$', '', [], [], true, 6.2, 24);
set(ax1, 'units', 'pixels');
pos = get(ax1, 'Position');
pos(1) = 50;
pos(2) = 100;
pos(3) = 1500;
pos(4) = 250;
set(ax1, 'Position', pos)

hgexport(fig, fullfile(outf, 'synth_validation'), hgexport('factorystyle'), 'Format', 'eps');