outf = 'D:\Arkivet\DNAdev\Plasmid paper\figures\paper_final';

pathConsensus = 'data\130kb.mat';
tmpFile = load(pathConsensus);
consensus = tmpFile.consensus;
consensus.lengthPx = length(consensus.barcode);
consensus.dotbars = arrayfun(@(i) Common.gen_dotbar(consensus.dots{i}, ...
  consensus.dotStd, consensus.lengthPx), 1:length(consensus.dots), 'un', 0);

pathResults = 'data\130kb_res.mat';
tmpFile = load(pathResults);
reference = tmpFile.reference;
refId = tmpFile.winnerIds(1);

import SignalRegistration.unmasked_pcc_corr

dims = [0 0 1600 600];
pad = 20;
width = dims(3)/4 - pad;
height = dims(4);
posx = pad + [0 width 2*width];
posy = [0 0 0];
mask = true;
fig = figure('Renderer', 'painters');
set(fig, 'position', dims)
for j=1:2
  barcodes = cellfun(@(x) x.stretchedBarcode,consensus.fragments{j},'UniformOutput',false);
  bitmasks = cellfun(@(x) x.stretchedBitmask,consensus.fragments{j},'UniformOutput',false);

  numBarcodes = length(barcodes);
  lengthPx = length(consensus.barcode);
  barsToPlot = cell(1, numBarcodes + 1);

  if length(consensus.labels{j}) > length(barcodes)
    [~, labSort] = sort(cellfun(@length, barcodes), 'descend');
  else
    [~, labSort] = sort(mod(consensus.labels{j} + any(consensus.labels{j} < 5)*10, length(consensus.barcode)));
  end
  for i=1:numBarcodes
      barcode = zscore(barcodes{labSort(i)});
      bitmask = logical(bitmasks{labSort(i)});
      xcorrs = unmasked_pcc_corr(barcode, consensus.barcode, bitmask);

      [~, linIdx] = max(xcorrs(:));
      [or, shift] = ind2sub(size(xcorrs), linIdx);

      if mask
        barcode(not(bitmask)) = nan;
      end
      
      if or == 2
        barcode = fliplr(barcode);
      end
      
      barcode = [barcode nan(1, length(consensus.barcode)-length(barcode))];
      bitmask = [bitmask false(1, length(consensus.barcode)-length(bitmask))];
      
      barsToPlot{i} = circshift(barcode, round(shift - .5));
  end

  barsToPlot{end} = zscore(consensus.barcode);
  ax = subplot(2,8, 2*(j - 1) + [1:2  9:10]);
  Export.Plot.concentric(fig, barsToPlot, consensus.labels{j}, consensus.dots{j}, 1, 0, 1, 1, 16, 'white', 'hot', strcat(char(96+j), ") ", consensus.enzymes{j}), j == 3);
  set(ax, 'units', 'pixels');
  pos = get(ax, 'Position');
  pos(1) = posx(j);
  pos(2) = posy(j);
  pos(3) = width;
  pos(4) = height;
  set(ax, 'Position', pos)
end


ax2 = subplot(2,8,5:8);
alignScore2 = Export.Plot.barcode_comparison(consensus.dotbars, reference.dotbars(refId,:), sets.camera.pxwnm/sets.lscale.meanbpnm, ...
  strcat("Overlap enzyme ", consensus.enzymes), 'dotcc', 'c) Sparsely labelled DNA barcode');
set(ax2, 'units', 'pixels');
pos = get(ax2, 'Position');
pos(1) = dims(3)/2;
pos(2) = height/2 + 4*pad;
pos(3) = dims(3)/2 - pad;
pos(4) = height/3 - pad;
set(ax2, 'Position', pos)
ax1 = subplot(2,8,13:16);
alignScore1 = Export.Plot.barcode_comparison(consensus.barcode, reference.barcodes{refId,:}, sets.camera.pxwnm/sets.lscale.meanbpnm, ...
  {'Experiment barcode', 'Theory barcode'}, 'pcc', 'd) Densely labelled DNA barcode');
set(ax1, 'units', 'pixels');
pos = get(ax1, 'Position');
pos(1) = dims(3)/2;
pos(2) = 4*pad;
pos(3) = dims(3)/2 - pad;
pos(4) = height/3 - pad;
set(ax1, 'Position', pos)

fig.PaperPositionMode = 'auto';
fig_pos = fig.PaperPosition;
fig.PaperSize = [fig_pos(3) fig_pos(4)];

hgexport(fig, fullfile(outf, 'consensus_130kb'), hgexport('factorystyle'), 'Format', 'eps');