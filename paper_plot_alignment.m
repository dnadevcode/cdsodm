outf = 'D:\Arkivet\DNAdev\Plasmid paper\figures\paper_finalsss';

pathConsensus = 'data\220kb.mat';
tmpFile = load(pathConsensus);
consensus = tmpFile.consensus;
consensus.lengthPx = length(consensus.barcode);
consensus.dotbars = arrayfun(@(i) Common.gen_dotbar(consensus.dots{i}, ...
  consensus.dotStd, consensus.lengthPx), 1:length(consensus.dots), 'un', 0);

pathResults = 'data\220kb_res.mat';
tmpFile = load(pathResults);
reference = tmpFile.reference;
refId = tmpFile.winnerIds(1);

%%
fig = figure('Renderer', 'painters');
dims = [1600 1200];
set(fig, 'Position', [[0, 0] dims]);
ax1 = subplot(2,1,1);
alignScore1 = Export.Plot.barcode_comparison(consensus.barcode, reference.barcodes{refId,:}, sets.camera.pxwnm/sets.lscale.meanbpnm, ...
  {'Experiment barcode', 'Theory barcode'}, 'pcc', 'a) Densely labelled DNA barcode (Competitive binding assay)');
set(ax1, 'units', 'pixels');
pos = get(ax1, 'Position');
pos(1) = 100;
pos(2) = 740;
pos(3) = 1450;
pos(4) = 400;
set(ax1, 'Position', pos)
ax2 = subplot(2,1,2);
alignScore2 = Export.Plot.barcode_comparison(consensus.dotbars, reference.dotbars(refId,:), sets.camera.pxwnm/sets.lscale.meanbpnm, ...
  strcat("Overlap enzyme ", consensus.enzymes), 'dotcc', 'b) Sparsely labelled DNA barcode (restriction-enzyme based assay)');
set(ax2, 'units', 'pixels');
pos = get(ax2, 'Position');
pos(1) = 100;
pos(2) = 140;
pos(3) = 1450;
pos(4) = 400;
set(ax2, 'Position', pos)
fig.PaperPositionMode = 'auto';
fig_pos = fig.PaperPosition;
fig.PaperSize = [fig_pos(3) fig_pos(4)];
hgexport(fig, fullfile(outf, 'alignment'), hgexport('factorystyle'), 'Format', 'eps');

%%
fig = figure;
dims = [1600 1200];
set(fig, 'Position', [[0, 0] dims]);
ax1 = subplot(2,1,1);
m = nanmean(allDcc(:));
s = nanstd(allDcc(:));
[m_fit, ~, s_fit] = Zeromodel.trunc_normal_ev_fit(allDcc(:), 0, 1, [0 1e-16 1e-16 1], [1 1e16 1 1e16], [m 1e-16 s 1], [false true false true], 1);
Export.Plot.score_distribution(allDcc(:), @(x) Zeromodel.trunc_normal_ev_pdf(x, m_fit, s_fit, 1), 100, 0:0.000001:1, ...
  '$D$', '', 'a)', {'Every alignment score for sparsely labelled barcodes', 'Truncated normal distribution'}, false, alignScore2, 36);
set(ax1, 'units', 'pixels');
pos = get(ax1, 'Position');
pos(1) = 50;
pos(2) = 740;
pos(3) = 1500;
pos(4) = 400;
set(ax1, 'Position', pos)
ax2 = subplot(2,1,2);
[a_fit, ~, n_fit] = Zeromodel.beta_ev_fit(allPcc(:), [4 10^-digits 1], [inf inf inf], [betaEVParams(1) 1 1], [false true false]);
Export.Plot.score_distribution(allPcc(:), @(x) Zeromodel.beta_ev_pdf(x, a_fit, 1, n_fit), 100, -1:0.0001:1, ...
  '$C$', '', 'b)', {'Every alignment score for densely labelled barcodes', 'Generalized beta distribution'}, false, alignScore1, 36);
set(ax2, 'units', 'pixels');
pos = get(ax2, 'Position');
pos(1) = 50;
pos(2) = 140;
pos(3) = 1500;
pos(4) = 400;
set(ax2, 'Position', pos)
fig.PaperPositionMode = 'auto';
fig_pos = fig.PaperPosition;
fig.PaperSize = [fig_pos(3) fig_pos(4)];
hgexport(fig, fullfile(outf, 'all_scores'), hgexport('factorystyle'), 'Format', 'eps');

%%
fig = figure;
dims = [1600 1200];
set(fig, 'Position', [[0, 0] dims]);
ax1 = subplot(2,1,1);
Export.Plot.score_distribution(squeeze(nanmax(allDcc, [], 3)), @(x) Zeromodel.trunc_normal_ev_pdf(x, normEVParams(1), normEVParams(2), normEVParams(3)), 100, 0:0.000001:1, ...
  '$\hat{D}$', '', 'a)', {'Optimal alignment scores for sparsely labelled barcodes', 'EV truncated normal distribution'}, false, alignScore2, 36);
set(ax1, 'units', 'pixels');
pos = get(ax1, 'Position');
pos(1) = 50;
pos(2) = 740;
pos(3) = 1500;
pos(4) = 400;
set(ax1, 'Position', pos)
ax2 = subplot(2,1,2);
Export.Plot.score_distribution(max(allPcc, [], 2), @(x) Zeromodel.beta_ev_pdf(x, betaEVParams(1), 1, betaEVParams(2)), 100, -1:0.0001:1, ...
  '$\hat{C}$', '', 'b)', {'Optimal alignment scores for densely labelled barcodes', 'EV generalized beta distribution'}, true, alignScore1, 36);
set(ax2, 'units', 'pixels');
pos = get(ax2, 'Position');
pos(1) = 50;
pos(2) = 140;
pos(3) = 1500;
pos(4) = 400;
set(ax2, 'Position', pos)
fig.PaperPositionMode = 'auto';
fig_pos = fig.PaperPosition;
fig.PaperSize = [fig_pos(3) fig_pos(4)];
hgexport(fig, fullfile(outf, 'best_scores'), hgexport('factorystyle'), 'Format', 'eps');