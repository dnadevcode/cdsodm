cbid = [1 625];
dotid = [622 2];%[3956 1344];
dims = [1600 1200];

outf = 'D:\Arkivet\DNAdev\Plasmid paper\figures\zeromodel';

fig = figure('Renderer', 'painters');
set(fig, 'Position', [[0, 0] dims]);
ax1 = subplot(2,1,1);
Export.Plot.barcode_comparison(consensus.barcode, randomBarcodes(cbid(1),:), sets.camera.pxwnm/sets.lscale.meanbpnm, ...
  {'Experiment barcode', 'Theory barcode'}, 'pcc', 'a)');
set(ax1, 'units', 'pixels');
pos = get(ax1, 'Position');
pos(1) = 100;
pos(2) = 740;
pos(3) = 1450;
pos(4) = 400;
set(ax1, 'Position', pos)
ax2 = subplot(2,1,2);
Export.Plot.barcode_comparison(consensus.barcode, randomBarcodes(cbid(2),:), sets.camera.pxwnm/sets.lscale.meanbpnm, ...
  {'Experiment barcode', 'Theory barcode'}, 'pcc', 'b)');
set(ax2, 'units', 'pixels');
pos = get(ax2, 'Position');
pos(1) = 100;
pos(2) = 140;
pos(3) = 1450;
pos(4) = 400;
set(ax2, 'Position', pos)
fig.PaperPositionMode = 'auto';
fig_pos = fig.PaperPosition;
fig.PaperSize = [fig_pos(3) fig_pos(4)];
hgexport(fig, fullfile(outf, 'randomcb'), hgexport('factorystyle'), 'Format', 'eps');

fig = figure;
set(fig, 'Position', [[0, 0] dims]);
ax1 = subplot(2,1,1);
Export.Plot.barcode_comparison(consensus.dotbars, arrayfun(@(i) squeeze(randomDotbars(dotid(1),i,:))', 1:length(consensus.dotbars), 'un', 0), sets.camera.pxwnm/sets.lscale.meanbpnm, ...
  strcat("Overlap enzyme ", consensus.enzymes), 'dotcc', 'a)');
set(ax1, 'units', 'pixels');
pos = get(ax1, 'Position');
pos(1) = 100;
pos(2) = 740;
pos(3) = 1450;
pos(4) = 400;
set(ax1, 'Position', pos)
ax2 = subplot(2,1,2);
Export.Plot.barcode_comparison(consensus.dotbars, arrayfun(@(i) squeeze(randomDotbars(dotid(2),i,:))', 1:length(consensus.dotbars), 'un', 0), sets.camera.pxwnm/sets.lscale.meanbpnm, ...
  strcat("Overlap enzyme ", consensus.enzymes), 'dotcc', 'b)');
set(ax2, 'units', 'pixels');
pos = get(ax2, 'Position');
pos(1) = 100;
pos(2) = 140;
pos(3) = 1450;
pos(4) = 400;
set(ax2, 'Position', pos)
fig.PaperPositionMode = 'auto';
fig_pos = fig.PaperPosition;
fig.PaperSize = [fig_pos(3) fig_pos(4)];
hgexport(fig, fullfile(outf, 'randomdot'), hgexport('factorystyle'), 'Format', 'eps');

