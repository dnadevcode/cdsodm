%% run_consensus_restriction_many_cuts
% Script for generating consensus of circular DNA that has been cut any
% number of times by one or more restriction enzymes

rng(1)

sets = ini2struct('default_settings.ini');
sets.bab.binWidth = 12;

consensusPath = 'D:\Arkivet\DNAdev\Plasmid paper\data\220 kb plasmid FW group\Full Plasmid';
fragPaths = {'D:\Arkivet\DNAdev\Plasmid paper\data\220 kb plasmid FW group\Plasmid fragments from digestion with Paci';
  'D:\Arkivet\DNAdev\Plasmid paper\data\220 kb plasmid FW group\Plasmid fragments from digestion with Pmei';
  'D:\Arkivet\DNAdev\Plasmid paper\data\220 kb plasmid FW group\Plasmid fragments from digestion with SgrDi'};
enzymes = {'PacI';
  'PmeI';
  'SgrDI'};

%% Example pre-gen consensus
disp('Pre-gen consensus')

consensus = Consensus.consensus_many_cuts(consensusPath, fragPaths, enzymes, sets);

[pathConsensusFile, pathConsensusFolder] = uiputfile('*.mat', 'Choose location for saving consensus', 'consensus.mat');
save(fullfile(pathConsensusFolder, pathConsensusFile), 'consensus', '-v7.3');