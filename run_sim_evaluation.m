inputPath = 'data\emulation results';
outputFolder = 'data\emulation results';
enzymes = {'BspQI'};
% enzymes = {'AjuI', 'AvrII', 'NheI', 'SnaBI', 'SpeI'};
enzymeIds = [1];
outputFilename = ['simstats_' char(join(enzymes(enzymeIds), '_')) '.csv'];

digits(64)

itemList = dir(fullfile(inputPath, '*.mat'));
names = cell(length(itemList), 1);
seqlength = zeros(length(itemList), 1);
numbelowpcb = zeros(length(itemList), 1);
numbelowpdots = zeros(length(itemList), 1);
numbelowpdual = zeros(length(itemList), 1);
numpasscb = zeros(length(itemList), 1);
numpassdots = zeros(length(itemList), 1);
numpassdual = zeros(length(itemList), 1);
%%
parfor i = 1:length(itemList)

  item = load(fullfile(itemList(i).folder, itemList(i).name));
  disp([i item.consensus.lengthPx])
  sets = item.sets;
  consensus = item.consensus;
  reference = item.reference;
  comparisons = item.comparisons;
  randomBarcodes = item.randomBarcodes;
  randomDotbars = item.randomDotbars;
  randomScoresBarcodes = item.randomScoresBarcodes;
  randomScoresDotbars = item.randomScoresDotbars;

  pvalsBarcodes = cell(1, length(comparisons.scoreDense));
  pvalsDotbars = cell(1, length(comparisons.scoreSparse));

  betaEVParams = Zeromodel.beta_ev_params(randomScoresBarcodes, consensus.lengthPx);

  denseScores = comparisons.scoreDense;
  for k = 1:length(pvalsBarcodes)
    bNonNegativeScores = denseScores{k} > 0;
    pvalsBarcodes{k} = ones(size(denseScores{k}));
    pvalsBarcodes{k}(bNonNegativeScores) = 1 - Zeromodel.beta_ev_cdf(denseScores{k}(bNonNegativeScores), betaEVParams(1), 1, betaEVParams(2));

    doExtraPrecision = pvalsBarcodes{k} == 0 & bNonNegativeScores;
    pvalsBarcodes{k}(doExtraPrecision) = double(vpa(1) - Zeromodel.beta_ev_cdf(denseScores{k}(doExtraPrecision), ...
      betaEVParams(1), ...
      1, ...
      betaEVParams(2), ...
      true));

  end
  
  numDotsExp = sum(cellfun(@(x) sum(x), consensus.dotbars(enzymeIds)));
  numDotsRef = sum(cellfun(@(x) sum(x), reference.dotbars(:, enzymeIds)), 2);
  numDotsRnd = sum(sum(randomDotbars(:, enzymeIds, :), 3), 2);
  sumDotsExp = sum(cellfun(@(x) sum(x.^2), consensus.dotbars(enzymeIds)));
  sumDotsRef = sum(cellfun(@(x) sum(x.^2), reference.dotbars(:, enzymeIds)), 2);
  sumDotsRnd = sum(sum(randomDotbars(:, enzymeIds, :).^2, 3), 2);
  sumDotsExp(numDotsExp < 2) = nan;
  sumDotsRef(numDotsRef < 2) = nan;
  sumDotsRnd(numDotsRnd < 2) = nan;

  sumCC = nansum(randomScoresDotbars(:, enzymeIds, :), 2);
  normCC = sumCC ./ sqrt(sumDotsRnd * sumDotsExp);
  normCC(normCC == 0) = nan;
  bestCC = squeeze(nanmax(normCC, [], 3));
  
%   figure;histogram(bestCC)

  meanCC = nanmean(normCC(:));
  startStdCC = nanstd(normCC(:));

  if not(isnan(meanCC))
    sparseScores = comparisons.scoreSparse;
    [m_fit, ~, s_fit, n_fit] = Zeromodel.trunc_normal_ev_fit(bestCC(not(isnan(bestCC))), 0, 1, [0 1e-16 1e-16 1], [1 1 1 1e16], [meanCC 1e-16 startStdCC 1], [false true false false], 1);
    
    dotsEVParams = [m_fit, s_fit, n_fit];

    for k = 1:length(pvalsDotbars)
      thisSumCC = nansum(vertcat(sparseScores{k, enzymeIds}), 1);
      thisNormCC = thisSumCC / sqrt(sumDotsRef(k) * sumDotsExp);
      pvalsDotbars{k} = 1 - Zeromodel.trunc_normal_ev_cdf(thisNormCC, m_fit, s_fit, n_fit, 0, 1);

      doExtraPrecision = pvalsDotbars{k} == 0;
      pvalsDotbars{k}(doExtraPrecision) = vpa(1) - Zeromodel.trunc_normal_ev_cdf(thisNormCC(doExtraPrecision), m_fit, s_fit, n_fit, 0, 1, true);

      pvalsDotbars{k} = double(pvalsDotbars{k});
      pvalsDotbars{k} = real(pvalsDotbars{k});

    end

  else
    dotsEVParams = nan(1, 3);

    for k = 1:length(pvalsDotbars)
      pvalsDotbars{k} = nan(1, 2 * consensus.lengthPx);
    end

  end

  numBelowThresh = zeros(1, 3);
  numWinners = zeros(1, 3);
  omega = [1 0 0.5];

  for j = 1:3

    if consensus.lengthPx >= 2 * sets.resampling.blockWidth
      [pvalsDenseResampled, ...
          pvalsSparseResampled] = Common.evaluate_best_matches(consensus.barcode, ...
        consensus.dotbars(enzymeIds), ...
        consensus.lengthPx, ...
        consensus.enzymes(enzymeIds), ...
        pvalsBarcodes, ...
        pvalsDotbars, ...
        reference.barcodes, ...
        reference.dotbars(:, enzymeIds), ...
        betaEVParams, ...
        dotsEVParams, ...
        omega(j), ...
        sets);
    else
      pvalsDenseResampled = [];
      pvalsSparseResampled = [];
    end

    pThreshWinners = Common.get_winners(pvalsBarcodes, ...
      pvalsDotbars, ...
      [], ...
      [], ...
      omega(j), ...
      sets);
    resamplingWinners = Common.get_winners(pvalsBarcodes, ...
      pvalsDotbars, ...
      pvalsDenseResampled, ...
      pvalsSparseResampled, ...
      omega(j), ...
      sets);
    numBelowThresh(j) = length(pThreshWinners);
    numWinners(j) = length(resamplingWinners);

    if numBelowThresh(j) > 0 && ~any(cellfun(@(x) strcmp(x, consensus.name), reference.names(pThreshWinners)))
      numBelowThresh(j) = -1;
    end

    if numWinners(j) > 0 && ~any(cellfun(@(x) strcmp(x, consensus.name), reference.names(resamplingWinners)))
      numWinners(j) = -1;
    end

  end

  names{i} = consensus.shorthand;
  seqlength(i) = reference.lengthBp(ismember(reference.names, consensus.name));
  numbelowpcb(i) = numBelowThresh(1);
  numbelowpdots(i) = numBelowThresh(2);
  numbelowpdual(i) = numBelowThresh(3);
  numpasscb(i) = numWinners(1);
  numpassdots(i) = numWinners(2);
  numpassdual(i) = numWinners(3);

end

writetable(table(names, ...
  seqlength, ...
  numbelowpcb, ...
  numbelowpdots, ...
  numbelowpdual, ...
  numpasscb, ...
  numpassdots, ...
  numpassdual), ...
  fullfile(outputFolder, outputFilename))
