%% Plot number of matches etc

enzymes = {'BspQI'};
% enzymes = {'AjuI', 'AvrII', 'NheI', 'SnaBI', 'SpeI'};
enzymeIds = [1];
enzymes = enzymes(enzymeIds);

fileInput = fullfile('data\emulation results\', ['simstats_' char(join(enzymes, '_')) '.csv']);
folderOutput = 'D:\Arkivet\DNAdev\Plasmid paper\figures\asdafaf';
fileOutput = fullfile(folderOutput, 'nummatches_nick_90_1.eps');

mkdir(folderOutput)

dims = [1000 1200];

fig = figure('Renderer', 'painters');
set(fig, 'Position', [[0, 0] dims]);

fs = 14;
lfs = 12;

minkbp = 50;
maxkbp = 800;

minlog = log(minkbp);
maxlog = log(maxkbp);

bars = 8;

sigma = 3;
exptype = {'dots';
        'cb';
        'dual'; };

matchtype = {'numbelowp'; 'numpass'};

panelid = [1 3 5 2 4 6 7 8];
panelpos = [ ...
  [.07 .80 .42 .18];
  [.07 .55 .42 .18];
  [.07 .30 .42 .18];
  [.57 .80 .42 .18];
  [.57 .55 .42 .18];
  [.57 .30 .42 .18];
  [.07 .05 .42 .18];
  [.57 .05 .42 .18]];
panellab = {'a) Sparse labels, significant match-score';
  'c) Dense labels, significant match-score';
  'e) Dual labels, significant match-score';
  'b) Sparse labels, passed resampling test';
  'd) Dense labels, passed resampling test';
  'f) Dual labels, passed resampling test';
  'g) \% unique match, significant match-score';
  'h) \% unique match, passed resampling test'};

colors = Export.distinguishable_colors(100);

black = colors(4, :);
red = colors(85, :);
green = colors(13, :);
blue = colors(27, :);
cyan = colors(47, :);
% magenta = colors(30,:);
brown = colors(39, :);
yellow = colors(6, :);
extra = colors(16, :);

data = readtable(fileInput);

allLengths = data.seqlength / 1000;
selectedIdx = allLengths > minkbp & allLengths < maxkbp;
allLengths = allLengths(selectedIdx);

unqFrac = zeros(2, 3, bars);

pid = 0;
for i = 1:length(matchtype)
for j = 1:length(exptype)
  mt = matchtype{i};
  expt = exptype{j};
  pid = pid + 1;
  
  subplot(4, 2, panelid(pid))

  numMatches = data{:, strcat(mt, expt)};
  numMatches = numMatches(selectedIdx);

  barcounts = zeros(bars, 7);
  legendText = {};
  [~, idx] = sort(numMatches);
  lengths = allLengths(idx);
  numMatchSorted = numMatches(idx);

  numMatchLong = numMatchSorted(lengths >= 100);
  fprintf(strcat(expt, " above 100 kbp unique fraction: %.3f\n"), sum(numMatchLong == 1) / length(numMatchLong))
  fprintf(strcat(expt, " above 100 kbp identified fraction: %.3f\n"), 1 - (sum(numMatchLong == -1) + sum(numMatchLong == 0)) / length(numMatchLong))

  numMatchShort = numMatchSorted(lengths < 100);
  fprintf(strcat(expt, " below 100 kbp unique fraction: %.3f\n"), sum(numMatchShort == 1) / length(numMatchShort))
  fprintf(strcat(expt, " below 100 kbp identified fraction: %.3f\n"), 1 - (sum(numMatchShort == -1) + sum(numMatchShort == 0)) / length(numMatchShort))

  for k = 1:length(numMatchSorted)
    bin = min(max(floor((log(lengths(k)) - minlog) / (maxlog - minlog) * bars), 0) + 1, bars);
    %     disp(num2str([lengths(i) log(lengths(i)/1000) bin], 3))
    if numMatchSorted(k) == -1
      n = 7;
    elseif numMatchSorted(k) == 0
      n = 6;
    elseif numMatchSorted(k) > 4
      n = 5;
    else
      n = numMatchSorted(k);
    end

    barcounts(bin, n) = barcounts(bin, n) + 1;
  end

  h = bar(barcounts, 'stacked');

  set(h(1), 'FaceColor', green);
  legendText{1} = '1 match';
  set(h(2), 'FaceColor', red);
  legendText{2} = '2 matches';
  set(h(3), 'FaceColor', blue);
  legendText{3} = '3 matches';
  set(h(4), 'FaceColor', cyan);
  legendText{4} = '4 matches';
  set(h(5), 'FaceColor', yellow);
  legendText{5} = '$\geq$5 matches';
%   set(h(6), 'FaceColor', yellow);
%   legendText{6} = '$\geq 6$ matches';
  set(h(6), 'FaceColor', black);
  legendText{6} = 'No match';
  set(h(7), 'FaceColor', extra);
  legendText{7} = 'Random matches';

  hold on
  xlabel('Size (kbp)', 'Interpreter', 'latex', 'FontSize', fs)
  ylabel('Count', 'Interpreter', 'latex', 'FontSize', fs)

  ax = gca;
  ticks = 1:bars;
  ticksx = round(exp(minlog:(maxlog - minlog) / bars:maxlog));
  ax.XTick = ticks;
  ax.XTickLabel = arrayfun(@(x) ['>' num2str(x, '%d')], ticksx, 'un', 0);
  ax.FontSize = fs;
  
%   ax = gca;
  ax.Position = panelpos(pid,:);

  xlim([.5 bars + .5]);

  legend(h, legendText, 'Interpreter', 'latex', 'FontSize', lfs)
  
  tit = title(panellab{pid}, 'Interpreter', 'latex', 'FontSize', fs);
  set(tit, 'HorizontalAlignment', 'left');
  set(tit, 'Position', get(tit, 'Position').*[0 1 1] + [.6 0 0]);
  
  unqFrac(i, j, :) = barcounts(:, 1) ./ sum(barcounts, 2);

%   for x = ax.XTick
%     thisStr = char(compose("%.0f%%", barcounts(x, 1) / sum(barcounts(x, :)) * 100));
%     text(x - .12 * length(thisStr), sum(barcounts(x, :)) + 18, thisStr, 'fontsize', fs, 'color', black, 'FontWeight', 'bold')
%     text(x - .12 * length(thisStr) + 0.012, sum(barcounts(x, :)) + 20, thisStr, 'fontsize', fs, 'color', green)
%   end
end
end

exlegend = {'Sparse labels'; 'Dense labels'; 'Dual labels'};

subplot(4, 2, 7)
plot(100*squeeze(unqFrac(1, :, :))', 'linewidth', 2)
hold on
xlabel('Size (kbp)', 'Interpreter', 'latex', 'FontSize', fs)
ylabel('\%', 'Interpreter', 'latex', 'FontSize', fs)
ax = gca;
ticks = 1:bars;
ticksx = round(exp(minlog:(maxlog - minlog) / bars:maxlog));
ax.XTick = ticks;
ax.XTickLabel = arrayfun(@(x) ['>' num2str(x, '%d')], ticksx, 'un', 0);
ax.FontSize = fs;
ax.Position = panelpos(7, :);
xlim([.5 bars + .5]);
tit = title(panellab{7}, 'Interpreter', 'latex', 'FontSize', fs);
set(tit, 'HorizontalAlignment', 'left');
set(tit, 'Position', get(tit, 'Position').*[0 1 1] + [.6 0 0]);
legend(exlegend, 'Interpreter', 'latex', 'FontSize', lfs, 'location', 'northwest')

subplot(4, 2, 8)
plot(100*squeeze(unqFrac(2, :, :))', 'linewidth', 2)
hold on
xlabel('Size (kbp)', 'Interpreter', 'latex', 'FontSize', fs)
ylabel('\%', 'Interpreter', 'latex', 'FontSize', fs)
ax = gca;
ticks = 1:bars;
ticksx = round(exp(minlog:(maxlog - minlog) / bars:maxlog));
ax.XTick = ticks;
ax.XTickLabel = arrayfun(@(x) ['>' num2str(x, '%d')], ticksx, 'un', 0);
ax.FontSize = fs;
ax.Position = panelpos(8, :);
xlim([.5 bars + .5]);
tit = title(panellab{8}, 'Interpreter', 'latex', 'FontSize', fs);
set(tit, 'HorizontalAlignment', 'left');
set(tit, 'Position', get(tit, 'Position').*[0 1 1] + [.6 0 0]);
legend(exlegend, 'Interpreter', 'latex', 'FontSize', lfs, 'location', 'northwest')


%   hold off

% fig.PaperPositionMode = 'auto';
% fig_pos = fig.PaperPosition;
% fig.PaperSize = [fig_pos(3) fig_pos(4)];

hgexport(fig, fileOutput, hgexport('factorystyle'), 'Format', 'eps');
