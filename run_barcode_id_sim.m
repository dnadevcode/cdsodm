%% run_barcode_id_sim
% Script for identifying dually labelled barcodes

%% Default settings
sets = ini2struct('default_settings.ini');
model = Common.cb_model();
enzymes = {'BspQI'};
% enzymes = {'AjuI', 'AvrII', 'NheI', 'SnaBI', 'SpeI'};

shortestLength = 5e4;
longestLength = 8e5;

pathOutput = 'data\emulation results\';
pathReference = {'D:\Arkivet\DNAdev\plasmids_FW';
  'D:\Arkivet\DNAdev\RefSeqUnpacked'};
pathTheory = 'data\pregen_theory.mat';
pathFFT = 'data\cb_zeromodel.mat';
pathLambda = 'data\J02459.1_Enterobacteria_phage_lambda.fasta';

%% Do free conc
disp('Computing free concentrations')
tic
[sets.freeconc.concY, ...
    sets.freeconc.concN] = Common.compute_free_conc(pathLambda, ...
  sets.freeconc.concY, ...
  sets.freeconc.concN, ...
  sets.freeconc.concDNA);
toc

%% Load reference sequences
disp('Load reference sequences')
tic
rawFasta = cellfun(@(x) Import.load_fastas_from_folder(x), pathReference, 'un', 0);
rawFasta = vertcat(rawFasta{:});

seqLengths = arrayfun(@(x) length(x.Sequence), rawFasta);
rawFasta = rawFasta(seqLengths >= shortestLength & seqLengths <= longestLength);
toc

%% Setup simulation
pregenBarcodes = load(pathTheory);
mkdir(pathOutput);

%%

for i=1:length(rawFasta)
    disp(num2str([i length(rawFasta(i).Sequence)]))
    
    %% Emulate experiment
    disp('Emulating experiment')
    tic
    consensus = struct();
    consensus.name = rawFasta(i).Header;
    consensus.shorthand = consensus.name(1:min([strfind(consensus.name, ' ')-1, ...
        length(consensus.name)]));
    consensus.enzymes = enzymes;
    [consensus.barcode, ...
        consensus.dots, ...
        consensus.dotbars, ...
        consensus.lengthPx] = Common.emulate_experiment(rawFasta(i).Sequence, ...
        enzymes, ...
        sets);
    toc
    
    %% Load reference barcodes
    disp('Load reference barcodes')
    tic
    reference = struct();
    [reference.barcodes, ...
        reference.dots, ...
        reference.rawDots, ...
        reference.dotbars, ...
        reference.names, ...
        reference.lengthBp] = Import.load_pregen_theory(pregenBarcodes, ...
        rawFasta, ...
        consensus, ...
        sets);
    toc
    
    %% Calculate match-scores
    disp('Calculating match-scores')
    tic
    comparisons = struct();
    comparisons.scoreDense = Common.compute_alignment_score_dense(consensus.barcode, ...
        reference.barcodes);
    comparisons.scoreSparse = Common.compute_alignment_score_sparse(consensus.dotbars, ...
        reference.dotbars);
    toc
    
    %% Generate random barcodes
    disp('Generating random barcodes')
    tic
    randomBarcodes = Zeromodel.gen_random_barcodes(consensus.lengthPx, ...
        sets.zeromodel.numrand, ...
        sets.camera.psfnm/sets.camera.pxwnm, ...
        pathFFT);
    toc
    disp('Generating random dotbars')
    tic
    randomDotbars = Zeromodel.gen_random_dotbars(reference.rawDots, ...
        reference.lengthBp, ...
        consensus.lengthPx, ...
        sets, ...
        sets.emulation.cutLocationStd);
    toc
    
    %%
    disp('Calculating random scores')
    tic
    randomScoresBarcodes = Zeromodel.compute_random_barcode_scores(consensus.barcode, ...
        randomBarcodes);
    randomScoresDotbars = Zeromodel.compute_random_dotbar_scores(consensus.dotbars, ...
        randomDotbars);
    toc
    
    %%
    save(fullfile(pathOutput, [consensus.shorthand '.mat']), ...
        'sets', ...
        'consensus', ...
        'reference', ...
        'comparisons', ...
        'randomBarcodes', ...
        'randomDotbars', ...
        'randomScoresBarcodes', ...
        'randomScoresDotbars', ...
        '-v7.3');
    
end

