
inpf = {'data\130kb_res.mat';
  'data\220kb_res.mat'};
tits = {'The 130 kbps plasmid'; 'The 220 kbps plasmid'};
outf = 'D:\Arkivet\DNAdev\Plasmid paper\figures\paper_final';
outname = 'results';

fig = figure('Renderer', 'painters');
set(fig, 'Position', [0, 0, 1600, 1200]);

for i=1:2
  
  tmpf = load(inpf{i});
  pvalsDense = tmpf.pvalsDense;
  pvalsSparse = tmpf.pvalsSparse;
  zSigma = tmpf.zSigma;
  zSigmaDense = tmpf.zSigmaDense;
  zSigmaSparse = tmpf.zSigmaSparse;

  zCombined = cellfun(@(x,y) (-norminv(x) - norminv(y)) / sqrt(2), pvalsDense, pvalsSparse, 'un', 0);
  zCombinedMax = cellfun(@max, zCombined);
  [zCombinedMaxMax, winnerId] = max(zCombinedMax);
  [~, winnerPos] = max(zCombined{winnerId});
  zDense = cellfun(@(x) max(-norminv(x)), pvalsDense);
  zSparse = cellfun(@(x) max(-norminv(x)), pvalsSparse);
  
  ax1 = subplot(3,2,1+i-1);
  Export.Plot.score_distribution(zSparse(:), [], 100, -11:0.0001:11, ...
    '$Z_\textrm{sparse}$', '', [char(96+i) ') ' tits{i}], [], ...
    true, max(zSparse), 24, max(zSparse), zSigmaSparse);
  set(ax1, 'units', 'pixels');
  pos = get(ax1, 'Position');
  pos(1) = 50 + (i-1) * 775;
  pos(2) = 900;
  pos(3) = 725;
  pos(4) = 250;
  set(ax1, 'Position', pos)
  ax2 = subplot(3,2,3+i-1);
  Export.Plot.score_distribution(zDense(:), [], 100, -11:0.0001:11, ...
    '$Z_\textrm{dense}$', '', [], [], ...
    true, max(zDense), 24, max(zDense), zSigmaDense);
  set(ax2, 'units', 'pixels');
  pos = get(ax2, 'Position');
  pos(1) = 50 + (i-1) * 775;
  pos(2) = 500;
  pos(3) = 725;
  pos(4) = 250;
  set(ax2, 'Position', pos)
  ax3 = subplot(3,2,5+i-1);
  Export.Plot.score_distribution(zCombinedMax(:), [], 100, -11:0.0001:11, ...
    '$Z_\textrm{dual}$', '', [], [], ...
    true, zCombinedMaxMax, 24, zCombinedMaxMax, zSigma);
  set(ax3, 'units', 'pixels');
  pos = get(ax3, 'Position');
  pos(1) = 50 + (i-1) * 775;
  pos(2) = 100;
  pos(3) = 725;
  pos(4) = 250;
  set(ax3, 'Position', pos)
  
  tmpzssorted = sort(zSparse, 'descend', 'missingplacement', 'last');
  tmpzdsorted = sort(zDense, 'descend', 'missingplacement', 'last');
  tmpzcsorted = sort(zCombinedMax, 'descend', 'missingplacement', 'last');
  
  fprintf("%g\t%g\t%g\n", normcdf(-max(zSparse)), normcdf(-max(zDense)), normcdf(-zCombinedMaxMax));
  fprintf("%g\t%g\t%g\n", (tmpzssorted(1) - tmpzssorted(2))/zSigmaSparse, (tmpzdsorted(1) - tmpzdsorted(2))/zSigmaDense, (tmpzcsorted(1) - tmpzcsorted(2))/zSigma)
end

annotation('line', [0.5 0.5], [0.02 0.98], 'Color', 'black', 'LineWidth', 1);

hgexport(fig, fullfile(outf, outname), hgexport('factorystyle'), 'Format', 'eps');